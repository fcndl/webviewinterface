/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class EditScanWrapperActivity extends AppCompatActivity {

    static final Integer EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 40122;
    static final Integer PICK_IMAGE_FILE = 1358;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        callEdit();
    }



    public void callEdit(){

        if(PermissionsUtils.checkPermissionForExternalStorage(this)) {

            // Llamar a ScanActivity con la preferencia para escanear tomando imagen desde galeria
            /*Intent intent = new Intent(this, ScanActivity.class);
            intent.putExtra("fileDesc", getIntent().getExtras().getString("fileDesc", ""));
            intent.putExtra("ppi", getIntent().getExtras().getString("ppi", ""));
            intent.putExtra("default_ppi", getIntent().getExtras().getString("default_ppi", ""));
            intent.putExtra("path", getIntent().getExtras().getString("path", ""));
            intent.putExtra("default_path", getIntent().getExtras().getString("default_path", ""));
            intent.putExtra("depthcolor", getIntent().getExtras().getString("depthcolor", ""));
            int preference = ScanConstants.OPEN_MEDIA;
            intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);
            startActivity(intent);
            finish();*/


            // Lanzar intent para abrir la galeria
            Intent intent = new Intent(Intent.ACTION_PICK,
                    android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            startActivityForResult(intent, PICK_IMAGE_FILE);




        } else {
            PermissionsUtils.requestPermissionForExternalStorage(this,EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }

    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    callEdit();
                    return;

                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay
                        finish();
                        return;
                    } else {
                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(this, true);
                        return;
                    }
                }
            }
        }
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==PICK_IMAGE_FILE && resultCode==Activity.RESULT_OK){
            Uri file_path = data.getData();
            Intent intent = new Intent();
            intent.setData(file_path);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra("pdf_uri", file_path);
            setResult(Activity.RESULT_OK, intent);
        }
        finish();

    }









}
