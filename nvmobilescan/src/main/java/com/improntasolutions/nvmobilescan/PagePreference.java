/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by flezcano on 27/11/2017.
 */

/*public class PagePreference {

    enum  PageOrientation  {Auto, Vertical, Horizontal}
    enum PageSize {Auto, A4, Legal, PostCard}


    PageOrientation pageOrientation;
    PageSize pageSize;


    public void setPageSize(PageSize ps){
        this.pageSize = ps;
    }

    public void setPageOrientation(PageOrientation po){
        this.pageOrientation = po;
    }

    public void setPageOrientation(Integer po){
        switch (po) {
            case 0:
                this.pageOrientation = PageOrientation.Auto;
                break;
            case 1:
                this.pageOrientation = PageOrientation.Vertical;
                break;
            case 2:
                this.pageOrientation = PageOrientation.Horizontal;
                break;
        }
    }

    public void setPageSize(Integer ps){
        switch (ps){
            case 0:
                this.pageSize = PageSize.Auto;
                break;
            case 1:
                this.pageSize = PageSize.A4;
                break;
            case 2:
                this.pageSize = PageSize.Legal;
                break;
            case 3:
                this.pageSize = PageSize.PostCard;
                break;
        }
    }

    public PageOrientation getPageOrientation(){
        return this.pageOrientation;
    }

    public PageSize getPageSize(){
        return this.pageSize;
    }

}*/

public class PagePreference implements Parcelable {

    public enum  PageOrientation  {Auto, Vertical, Horizontal}
    public  enum PageSize {Auto, A4, Legal, PostCard}


    PageOrientation pageOrientation;
    PageSize pageSize;

    public  PagePreference(){
    }

    public void setPageSize(PageSize ps){
        this.pageSize = ps;
    }

    public void setPageOrientation(PageOrientation po){
        this.pageOrientation = po;
    }

    public void setPageOrientation(Integer po){
        switch (po) {
            case 0:
                this.pageOrientation = PageOrientation.Auto;
                break;
            case 1:
                this.pageOrientation = PageOrientation.Vertical;
                break;
            case 2:
                this.pageOrientation = PageOrientation.Horizontal;
                break;
        }
    }

    public void setPageSize(Integer ps){
        switch (ps){
            case 0:
                this.pageSize = PageSize.Auto;
                break;
            case 1:
                this.pageSize = PageSize.A4;
                break;
            case 2:
                this.pageSize = PageSize.Legal;
                break;
            case 3:
                this.pageSize = PageSize.PostCard;
                break;
        }
    }

    public PageOrientation getPageOrientation(){
        return this.pageOrientation;
    }

    public PageSize getPageSize(){
        return this.pageSize;
    }


    protected PagePreference(Parcel in) {
        //pageOrientation = (PageOrientation) in.readValue(PageOrientation.class.getClassLoader());
        //pageSize = (PageSize) in.readValue(PageSize.class.getClassLoader());

        try {
            pageOrientation = PageOrientation.valueOf(in.readString());
        } catch (IllegalArgumentException x) {
            pageOrientation = null;
        }
        try {
            pageSize = PageSize.valueOf(in.readString());
        } catch (IllegalArgumentException x) {
            pageSize = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString((pageOrientation == null) ? "" : pageOrientation.name());
        dest.writeString((pageSize == null) ? "" : pageSize.name());
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PagePreference> CREATOR = new Parcelable.Creator<PagePreference>() {
        @Override
        public PagePreference createFromParcel(Parcel in) {
            return new PagePreference(in);
        }

        @Override
        public PagePreference[] newArray(int size) {
            return new PagePreference[size];
        }
    };
}
