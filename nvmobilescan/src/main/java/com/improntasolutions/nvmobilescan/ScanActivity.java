/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;



import java.util.ArrayList;

/**
 * Created by jhansi on 28/03/15.
 */
public class ScanActivity extends AppCompatActivity implements IScanner {

    public ArrayList<String> uriList = new ArrayList<String>();
    public ArrayList<String> originalUriList = new ArrayList<String>();
    public ArrayList<PagePreference> pagesPreferences = new ArrayList<PagePreference>();
    public String fileDesc = "";

    public static String PREFS_NAME = "nvmobilescan_prefs";

    private boolean wasDestroyed = false;
    public boolean intentPreferenceAlreadyHandled = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.scan_layout);

        fileDesc = getIntent().getExtras().getString("fileDesc", "");

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("wasDestroyed")) {
                wasDestroyed = savedInstanceState.getBoolean("wasDestroyed", false);
                uriList = savedInstanceState.getStringArrayList("uri_list");
                originalUriList = savedInstanceState.getStringArrayList("original_uri_list");
                pagesPreferences = savedInstanceState.getParcelableArrayList("pages_preferences");
                intentPreferenceAlreadyHandled = savedInstanceState.getBoolean("intentPreferenceAlreadyHandled", false);
            }
        }

        if(!wasDestroyed) {
            init();
        }
    }

    private void init() {

        // limpiar cache para poder agregar nuevas
        // imagenes y evitar problema de espacio insuficiente
        if (getIntent().getExtras().getBoolean("clearCacheFolderOnInit", true)) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Utils.clearCacheFolder(getApplicationContext());
                }
            }).start();
        }

        // cargar documentos si se ingresó en modo edición
        String docEditId = getIntent().getExtras().getString("doc_edit_id", "");
        if (!docEditId.equals("")) {
            //uriList = Utils.getDocEditUrls(docEditId,this);
            String[] strArr = new String[1]; strArr[0] = "";
            Utils.getDocEditUrls(docEditId, uriList, originalUriList, pagesPreferences, strArr, this);
            fileDesc = strArr[0];
        }

        PickImageFragment fragment = new PickImageFragment();
        Bundle bundle = new Bundle();
        bundle.putInt(ScanConstants.OPEN_INTENT_PREFERENCE, getPreferenceContent());
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        fragmentTransaction.add(R.id.content, fragment,"pick_fragment");
        //fragmentTransaction.addToBackStack(PickImageFragment.class.toString());

        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }


    protected int getPreferenceContent() {
        return getIntent().getIntExtra(ScanConstants.OPEN_INTENT_PREFERENCE, 0);
    }



    // NO SE USA
    @Override
    public void onBitmapSelect(Uri uri) {
        ScanFragment fragment = new ScanFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SELECTED_BITMAP, uri);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.addToBackStack(ScanFragment.class.toString());
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onBitmapSelect2(Uri uri, int rotation) {
        ScanFragment fragment = new ScanFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SELECTED_BITMAP, uri);
        bundle.putInt("rotation", rotation);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.replace(R.id.content, fragment,"scan_fragment");

        fragmentTransaction.addToBackStack(ScanFragment.class.toString());
        //fragmentTransaction.commit();
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void onScanFinish(Uri uri) {
        ResultFragment fragment = new ResultFragment();
        Bundle bundle = new Bundle();
        bundle.putParcelable(ScanConstants.SCANNED_RESULT, uri);
        fragment.setArguments(bundle);
        FragmentManager fragmentManager = getSupportFragmentManager();

        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.replace(R.id.content, fragment);

        fragmentTransaction.addToBackStack(ResultFragment.class.toString());
        fragmentTransaction.commitAllowingStateLoss();
    }



    public void editImage(int index) {
        ResultFragment fragment = new ResultFragment();
        Bundle bundle = new Bundle();

        Uri uri = Uri.parse(uriList.get(index));
        bundle.putParcelable(ScanConstants.SCANNED_RESULT, uri);
        bundle.putInt("uri_index", index);
        fragment.setArguments(bundle);

        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        //fragmentTransaction.add(R.id.content, fragment);
        fragmentTransaction.replace(R.id.content, fragment);


        fragmentTransaction.addToBackStack(ResultFragment.class.toString());
        fragmentTransaction.commitAllowingStateLoss();
    }

    @Override
    public void showResult() {

        FragmentManager fm = getSupportFragmentManager();
        try {
            fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);

            //fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            PickImageFragment pickImageFragment = (PickImageFragment)
                    fm.findFragmentByTag("pick_fragment");
            pickImageFragment.loadImages();



            /*fm.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            //fm.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
            PickImageFragment fragment = new PickImageFragment();
            Bundle bundle = new Bundle();
            fragment.setArguments(bundle);
            FragmentTransaction fragmentTransaction = fm.beginTransaction();

            fragmentTransaction.add(R.id.content, fragment,"pick_fragment");
            fragmentTransaction.addToBackStack(PickImageFragment.class.toString());
            fragmentTransaction.commitAllowingStateLoss();*/


            //IR CON REPLACE


        } catch(Exception e){

            /*PickImageFragment fragment = new PickImageFragment();
            Bundle bundle = new Bundle();
            bundle.putInt(ScanConstants.OPEN_INTENT_PREFERENCE, 0);
            fragment.setArguments(bundle);
            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.add(R.id.content, fragment);
            fragmentTransaction.commitAllowingStateLoss();*/

        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("wasDestroyed", true);
        outState.putStringArrayList("uri_list", uriList);
        outState.putStringArrayList("original_uri_list", originalUriList);
        outState.putParcelableArrayList("pages_preferences", pagesPreferences);
        outState.putBoolean("intentPreferenceAlreadyHandled", intentPreferenceAlreadyHandled);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }



    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)  {
        if (Integer.parseInt(android.os.Build.VERSION.SDK) > 5
                && keyCode == KeyEvent.KEYCODE_BACK
                && event.getRepeatCount() == 0) {
            onBackPressed();
            return true;
        }

        // cualquier otra tecla:
        return super.onKeyDown(keyCode, event);
    }

    @Override
    public void onBackPressed() {

        // Si vuelve a la pantalla anterior guardar el estado de los archivos subidos
        FragmentManager fm = getSupportFragmentManager();
        PickImageFragment pickImageFragment = (PickImageFragment)
                fm.findFragmentByTag("pick_fragment");


        ScanFragment scanFragment = (ScanFragment)
                fm.findFragmentByTag("scan_fragment");



        if(scanFragment!=null && scanFragment.isVisible() && uriList.size()==0 && getPreferenceContent()!=0){
            // si escaneo imagen, decidio volver para atras
            // y no hay archivos en la lista, devolverlo
            // a la funcion de scan que habia seleccionado (file explorer o scan de camara)



            /*
            // 1 - ir a pickimage...
            super.onBackPressed();


            // 2 - reload pickImageFragment
            PickImageFragment frg = null;
            frg = (PickImageFragment)getSupportFragmentManager().findFragmentByTag("pick_fragment");
            frg.handleIntentPreference();
            //final FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            //ft.detach(frg);
            //ft.attach(frg);
            //ft.commit();
            */


            // Relanzar la actividad
            //recreate();
            Intent intent = getIntent();
            finish();
            startActivity(intent);



            return;

        }



        // si estoy en un fragment posterior a picimage, el back se comporta
        // de manera normal
        if (pickImageFragment == null || !pickImageFragment.isVisible()) {
            // add your code here
            super.onBackPressed(); //quita el fragment actual del stack
            return;
        }


        // si se esta en el fragment pickimage con el modo action activado
        // hay que retornar (se desactiva el modo action)
        if(pickImageFragment!=null && pickImageFragment.mActionMode!=null){
            return; // en el onKeyPress el sistema delega quitar la barra de action mode
        }




        if(uriList.size()!=0) {
            new AlertDialog.Builder(ScanActivity.this)
                    .setTitle("")
                    .setMessage("Si continua perderá su progreso. Desea volver?")
                    .setPositiveButton("Sí, volver", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                        }
                    })
                    .setNegativeButton("No, continuar aquí.", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                        }
                    })
                    .show();
        } else {
            finish();
        }

        /*new AlertDialog.Builder(me)
                .setTitle("Advertencia")
                .setMessage("Si vuelve al menú principal perderá su progreso. Desea realmente salir?")
                .setPositiveButton("Sí, volver", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setNegativeButton("No, continuar aquí.", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .setCancelable(false)
                .show();*/
    }



    public native Bitmap getScannedBitmap(Bitmap bitmap, float x1, float y1, float x2, float y2, float x3, float y3, float x4, float y4);

    public native Bitmap getGrayBitmap(Bitmap bitmap);

    public native Bitmap getMagicColorBitmap(Bitmap bitmap);

    public native Bitmap getBWBitmap(Bitmap bitmap);

    public native float[] getPoints(Bitmap bitmap);

    static {
        System.loadLibrary("opencv_java3");
        System.loadLibrary("Scanner");
    }
}