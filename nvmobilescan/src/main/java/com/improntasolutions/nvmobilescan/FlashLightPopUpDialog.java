/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Dialog;

import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;


import android.content.Context;


import com.github.rongi.rotate_layout.layout.RotateLayout;


import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;


public class FlashLightPopUpDialog extends DialogFragment {

    String title;
    String message;
    String button_text;

    public View content;
    public int width;
    public int height;
    public int rotation;



    CameraCaptureActivity parent;

    public FlashLightPopUpDialog()
    {
        //setStyle(R.style.Dialog,getTheme());
        //setStyle(DialogFragment.STYLE_NO_TITLE, R.style.);
        setCancelable(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        /*Dialog dialog = new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                // si presiona back en el dialog debe volver al home
                // Esto es para limpiar la pila de activitis antes de volver
                //Intent intent = new Intent(getActivity(), HomeActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                //startActivity(intent);
                //getActivity().finish(); // call this to finish the current activity
            }
        };*/


        // request a window without the title
        //dialog.getWindow().setLayout(600, 600);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(
        //        new ColorDrawable(android.graphics.Color.TRANSPARENT));


        //WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        //params.width = 600;
        //params.height =  600;
        //params.gravity = Gravity.LEFT;
        //dialog.getWindow().setAttributes(params);

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.title = (String) args.getSerializable("title");
        this.message = (String) args.getSerializable("message");
        this.button_text = (String) args.getSerializable("button_text");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        //View popUpView =  inflater.inflate(R.layout.popup_flashlight_options,null);
        /*ScrollView sv = new ScrollView( super.getActivity());
        sv.setLayoutParams(new ScrollView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        sv.addView(content);*/

        SharedPreferences settings = getActivity().getApplicationContext().getSharedPreferences("scan_preferences_file", getActivity().getApplicationContext().MODE_PRIVATE);
        int selectFlashMode = settings.getInt("selected_flash_mode",2);

        View content =  inflater.inflate(R.layout.popup_flashlight_options,null);

        // desactivado
        RadioButton rb1 = (RadioButton)content.findViewById(R.id.rb1);
        if(selectFlashMode==0){
            rb1.setChecked(true);
        }

        // acivado
        RadioButton rb2 = (RadioButton)content.findViewById(R.id.rb2);
        if(selectFlashMode==1){
            rb2.setChecked(true);
        }

        //auto
        RadioButton rb3 = (RadioButton)content.findViewById(R.id.rb3);
        if(selectFlashMode==2){
            rb3.setChecked(true);
        }

        //torch
        RadioButton rb4 = (RadioButton)content.findViewById(R.id.rb4);
        if(selectFlashMode==3){
            rb4.setChecked(true);
        }


        RotateLayout rl = new RotateLayout(super.getActivity());
        //content.setLayoutParams(new ViewGroup.LayoutParams(WRAP_CONTENT,WRAP_CONTENT));
        rl.setLayoutParams(new ViewGroup.LayoutParams(WRAP_CONTENT,WRAP_CONTENT));
        rl.addView(content);
        rl.setAngle(rotation);



        return rl;
    }


    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }


    @Override
    public void onSaveInstanceState(Bundle b){
        b.putString("title", title);
        b.putString("message", message);
        b.putString("button_text", button_text);
    }





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Instantiate the GenericDialogListener so we can send events to the host
            parent = (CameraCaptureActivity) super.getActivity();
        } catch (Exception e) {
            Log.e("FlashLighPopUpDialog", e.getMessage());
        }
    }



    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();

        //int h = dialog.getWindow().getAttributes().height;
        if (dialog != null) {
                dialog.getWindow().setLayout(WRAP_CONTENT, WRAP_CONTENT);

        }
    }



}
