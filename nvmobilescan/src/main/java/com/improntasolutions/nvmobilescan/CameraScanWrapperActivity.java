/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class CameraScanWrapperActivity extends AppCompatActivity {

    static final int CAMERA_PERMISSION_REQUEST_CODE = 1050;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callScan();
    }

    public void callScan(){

        if (PermissionsUtils.checkPermissionForCamera(this)) {
            Intent intent = new Intent(this, ScanActivity.class);
            intent.putExtra("fileDesc", getIntent().getExtras().getString("fileDesc", "Documento a adjuntar"));
            intent.putExtra("ppi", getIntent().getExtras().getInt("ppi", 0));
            intent.putExtra("default_ppi", getIntent().getExtras().getInt("default_ppi", 0));
            intent.putExtra("path", getIntent().getExtras().getString("path", ""));
            intent.putExtra("default_path", getIntent().getExtras().getString("default_path", ""));
            intent.putExtra("depthcolor", getIntent().getExtras().getString("depthcolor", ""));
            intent.putExtra("clearCacheFolderOnInit", getIntent().getExtras().getBoolean("clearCacheFolderOnInit", true));
            intent.putExtra("doc_edit_id", getIntent().getExtras().getString("doc_edit_id", ""));

            //int preference = ScanConstants.OPEN_CAMERA;
            //intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            intent.addFlags(Intent.FLAG_ACTIVITY_FORWARD_RESULT);

            startActivity(intent);
            finish();
        } else {
            PermissionsUtils.requestPermissionForCamera(this, CAMERA_PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    callScan();
                    return;

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay
                        finish();
                        return;
                    } else {

                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(this, true);

                        return;
                    }
                }
            }
        }
    }

















}
