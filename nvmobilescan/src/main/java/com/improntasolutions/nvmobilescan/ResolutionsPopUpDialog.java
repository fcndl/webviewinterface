/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;


import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;


import android.content.Context;

import com.github.rongi.rotate_layout.layout.RotateLayout;

import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;


public class ResolutionsPopUpDialog extends DialogFragment {

    String title;
    String message;
    String button_text;

    public View content;
    public int width;
    public int height;
    public int rotation;


    CameraCaptureActivity parent;

    public ResolutionsPopUpDialog()
    {
        //setStyle(R.style.Dialog,getTheme());
        //setStyle(DialogFragment.STYLE_NO_TITLE, R.style.);
        setCancelable(true);
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        /*Dialog dialog = new Dialog(getActivity(), getTheme()){
            @Override
            public void onBackPressed() {
                // si presiona back en el dialog debe volver al home
                // Esto es para limpiar la pila de activitis antes de volver
                //Intent intent = new Intent(getActivity(), HomeActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                //startActivity(intent);
                //getActivity().finish(); // call this to finish the current activity
            }
        };*/


        // request a window without the title
        //dialog.getWindow().setLayout(600, 600);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        //dialog.getWindow().setBackgroundDrawable(
        //        new ColorDrawable(android.graphics.Color.TRANSPARENT));


        //WindowManager.LayoutParams params = dialog.getWindow().getAttributes();
        //params.width = 600;
        //params.height =  600;
        //params.gravity = Gravity.LEFT;
        //dialog.getWindow().setAttributes(params);

        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.title = (String) args.getSerializable("title");
        this.message = (String) args.getSerializable("message");
        this.button_text = (String) args.getSerializable("button_text");

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        /*ScrollView sv = new ScrollView( super.getActivity());
        sv.setLayoutParams(new ScrollView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT));
        sv.addView(content);*/



        RotateLayout rl = new RotateLayout(super.getActivity());
        //content.setLayoutParams(new ViewGroup.LayoutParams(WRAP_CONTENT,WRAP_CONTENT));
        rl.setLayoutParams(new ViewGroup.LayoutParams(WRAP_CONTENT,WRAP_CONTENT));
        //content.getParent().get
        if(content!=null) {
            rl.addView(content);
        }
        rl.setAngle(rotation);

        return rl;
    }





    @Override
    public void onSaveInstanceState(Bundle b){
    }





    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            // Instantiate the GenericDialogListener so we can send events to the host
            parent = (CameraCaptureActivity) super.getActivity();
        } catch (Exception e) {
            Log.e("FlashLighPopUpDialog", e.getMessage());
        }
    }



    @Override
    public void onStart()
    {
        super.onStart();
        Dialog dialog = getDialog();

        if(content==null) { // para que en el oncreate, no aparezca el dialog vacio
            try {
                dismissAllowingStateLoss();
                return;
            } catch(Exception e){
            }
        }

        //int h = dialog.getWindow().getAttributes().height;
        if (dialog != null) {
            if (rotation == 90 || rotation == 270) {
                dialog.getWindow().setLayout(height, width);
            } else {
                dialog.getWindow().setLayout(width, height);
            }
            /*int width   = getResources().getDimensionPixelSize(R.dimen.popup_width);
            int height = getResources().getDimensionPixelSize(R.dimen.popup_height);
            getDialog().getWindow().setLayout(width, height);*/

        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
        try {
            ((ViewGroup) content.getParent()).removeView(content);
        }catch (Exception e) {
        }
    }


}
