/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;



public class PageOrientationPrefDialog extends DialogFragment {

    String title;

    public PageOrientationPrefDialog() {
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.title = (String) args.getSerializable("title");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);





        return dialog;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.page_orientation_pref, container, false);
        TextView tvTitle = root.findViewById(R.id.title);

        if(savedInstanceState!=null){
            this.title = savedInstanceState.getString("title","");
        }
        tvTitle.setText(title);


        RadioGroup rGroup = (RadioGroup)root.findViewById(R.id.rgroup);


        PagePreference.PageOrientation  pageOri= ((ResultFragment) getTargetFragment()).pagePreference.getPageOrientation();
        RadioButton rb1=null;

        if(pageOri == PagePreference.PageOrientation.Auto){
            rGroup.check(R.id.rbAuto);
            //rb1 = (RadioButton) rGroup.findViewById(R.id.rbAuto);
        }
        if(pageOri == PagePreference.PageOrientation.Vertical){
            rGroup.check(R.id.rbVertical);
            //rb1 = (RadioButton) rGroup.findViewById(R.id.rbVertical);
        }
        if(pageOri == PagePreference.PageOrientation.Horizontal){
            //rb1 = (RadioButton) rGroup.findViewById(R.id.rbHorizontal);
            rGroup.check(R.id.rbHorizontal);
        }




        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                if (checkedId == R.id.rbAuto) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageOrientation(PagePreference.PageOrientation.Auto);
                }
                if (checkedId == R.id.rbVertical) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageOrientation(PagePreference.PageOrientation.Vertical);
                }
                if (checkedId == R.id.rbHorizontal) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageOrientation(PagePreference.PageOrientation.Horizontal);
                }
                dismiss();
                return;
            }
        });










        /*switch (pageOri){
            case Auto:
                rb1 = (RadioButton) rGroup.findViewById(R.id.rbAuto);
                rb1.setChecked(true);
                break;
            case Vertical:
                rb1 = (RadioButton)rGroup.findViewById(R.id.rbVertical);
                rb1.setChecked(true);
                break;
            case Horizontal:
                rb1 = (RadioButton)rGroup.findViewById(R.id.rbHorizontal);
                rb1.setChecked(true);
                break;
        }*/

        /*final RadioButton rb2 = rb1;
        if(rb1!=null) {
            rb1.post(new Runnable() {
                @Override
                public void run() {
                    rb2.setChecked(true);
                }
            });
        }*/

        return root;
    }


    @Override
    public void onStart() {
        super.onStart();

        /*RadioGroup rGroup = (RadioGroup) getView().findViewById(R.id.rgroup);

        PagePreference.PageOrientation  pageOri= ((ResultFragment) getTargetFragment()).pagePreference.getPageOrientation();
        RadioButton rb1=null;

        if(pageOri == PagePreference.PageOrientation.Auto){
            rGroup.check(R.id.rbAuto);
            //rb1 = (RadioButton) rGroup.findViewById(R.id.rbAuto);
        }
        if(pageOri == PagePreference.PageOrientation.Vertical){
            rGroup.check(R.id.rbVertical);
            //rb1 = (RadioButton) rGroup.findViewById(R.id.rbVertical);
        }
        if(pageOri == PagePreference.PageOrientation.Horizontal){
            //rb1 = (RadioButton) rGroup.findViewById(R.id.rbHorizontal);
            rGroup.check(R.id.rbHorizontal);
        }*/
    }

    @Override
    public void onSaveInstanceState(Bundle b){
        b.putString("title", title);
    }

}
