/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Activity;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.PointF;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;



import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ScanFragment extends Fragment {
    private static final String TAG = "ScanFragment";
    private Button scanButton;
    private ImageView sourceImageView;
    private FrameLayout sourceFrame;
    private PolygonView polygonView;
    private View view;
    private IScanner scanner;

    boolean procBusy;



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof IScanner)) {
            throw new ClassCastException("Activity must implement IScanner");
        }
        this.scanner = (IScanner) activity;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.scan_fragment_layout, null);
        init();
        return view;
    }

    public ScanFragment() {
    }

    private void init() {

        sourceImageView = (ImageView) view.findViewById(R.id.sourceImageView);
        scanButton = (Button) view.findViewById(R.id.scanButton);
        scanButton.setOnClickListener(new ScanButtonClickListener());
        sourceFrame = (FrameLayout) view.findViewById(R.id.sourceFrame);
        polygonView = (PolygonView) view.findViewById(R.id.polygonView);
        sourceFrame.post(new Runnable() {
            @Override
            public void run() {
                setBitmapOnImageView();
            }
        });

    }


    private  void setBitmapOnImageView(){

        try {
            Bitmap ori = null;
            Bitmap scaled = null;
            Bitmap bitmapFixed = null;

            Uri uri = getArguments().getParcelable(ScanConstants.SELECTED_BITMAP);
            Integer   rotation = getArguments().getInt("rotation");

            switch (rotation) {
                case 0: default:

                    ori = Utils.getThumbBitmap(getActivity(),uri,sourceFrame.getWidth(), sourceFrame.getHeight());
                    scaled = Utils.scaledBitmap(ori, sourceFrame.getWidth(), sourceFrame.getHeight());
                    bitmapFixed = scaled;
                    break;
                case 90:
                    ori = Utils.getThumbBitmap(getActivity(),uri,sourceFrame.getHeight(), sourceFrame.getWidth());
                    scaled = Utils.scaledBitmap(ori, sourceFrame.getHeight(), sourceFrame.getWidth());
                    bitmapFixed = rotateImage(scaled, 90.0f);
                    break;
                case 180:

                    ori = Utils.getThumbBitmap(getActivity(),uri,sourceFrame.getWidth(), sourceFrame.getHeight());
                    scaled = Utils.scaledBitmap(ori, sourceFrame.getWidth(), sourceFrame.getHeight());
                    bitmapFixed = rotateImage(scaled, 180.0f);
                    break;
                case 270:
                    ori = Utils.getThumbBitmap(getActivity(),uri,sourceFrame.getHeight(), sourceFrame.getWidth());
                    scaled = Utils.scaledBitmap(ori, sourceFrame.getHeight(), sourceFrame.getWidth());
                    bitmapFixed = rotateImage(scaled, 270.0f);
                    break;
            }

            if(ori!=scaled){
                ori.recycle();
            }
            if(scaled!=bitmapFixed){
                scaled.recycle();
            }

            sourceImageView.setImageBitmap(bitmapFixed);
            Bitmap tempBitmap = ((BitmapDrawable) sourceImageView.getDrawable()).getBitmap();
            Map<Integer, PointF> pointFs = getEdgePoints(tempBitmap);
            polygonView.setPoints(pointFs);
            polygonView.setVisibility(View.VISIBLE);
            int padding = (int) getResources().getDimension(R.dimen.scanPadding);
            FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(tempBitmap.getWidth() + 2 * padding, tempBitmap.getHeight() + 2 * padding);
            layoutParams.gravity = Gravity.CENTER;
            polygonView.setLayoutParams(layoutParams);

        } catch (Exception e) {
            Log.e(TAG, Utils.getErrorMsg(e));
        }

    }

    private Uri getUri() {
        Uri uri = getArguments().getParcelable(ScanConstants.SELECTED_BITMAP);
        return uri;
    }

    private Integer getRotation(){
        Integer rotation = getArguments().getInt("rotation");
        return  rotation;
    }

    public  Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
                true);
    }

    private Map<Integer, PointF> getEdgePoints(Bitmap tempBitmap) {
        List<PointF> pointFs = getContourEdgePoints(tempBitmap);
        Map<Integer, PointF> orderedPoints = orderedValidEdgePoints(tempBitmap, pointFs);
        return orderedPoints;
    }

    private List<PointF> getContourEdgePoints(Bitmap tempBitmap) {
        float[] points = ((ScanActivity) getActivity()).getPoints(tempBitmap);
        float x1 = points[0];
        float x2 = points[1];
        float x3 = points[2];
        float x4 = points[3];

        float y1 = points[4];
        float y2 = points[5];
        float y3 = points[6];
        float y4 = points[7];

        List<PointF> pointFs = new ArrayList<>();
        pointFs.add(new PointF(x1, y1));
        pointFs.add(new PointF(x2, y2));
        pointFs.add(new PointF(x3, y3));
        pointFs.add(new PointF(x4, y4));
        return pointFs;
    }

    private Map<Integer, PointF> getOutlinePoints(Bitmap tempBitmap) {
        Map<Integer, PointF> outlinePoints = new HashMap<>();
        outlinePoints.put(0, new PointF(0, 0));
        outlinePoints.put(1, new PointF(tempBitmap.getWidth(), 0));
        outlinePoints.put(2, new PointF(0, tempBitmap.getHeight()));
        outlinePoints.put(3, new PointF(tempBitmap.getWidth(), tempBitmap.getHeight()));
        return outlinePoints;
    }

    private Map<Integer, PointF> orderedValidEdgePoints(Bitmap tempBitmap, List<PointF> pointFs) {
        Map<Integer, PointF> orderedPoints = polygonView.getOrderedPoints(pointFs);
        if (!polygonView.isValidShape(orderedPoints)) {
            orderedPoints = getOutlinePoints(tempBitmap);
        }
        return orderedPoints;
    }


    private void showErrorDialog() {
        SingleButtonDialogFragment fragment = new SingleButtonDialogFragment(R.string.ok, getString(R.string.cantCrop), "Error", true);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        fragment.show(fm, SingleButtonDialogFragment.class.toString());
    }

    private boolean isScanPointsValid(Map<Integer, PointF> points) {
        return points.size() == 4;
    }


    private Bitmap getScannedBitmap(Bitmap original, Map<Integer, PointF> points) {
        int width = original.getWidth();
        int height = original.getHeight();
        float xRatio = (float) original.getWidth() / sourceImageView.getWidth();
        float yRatio = (float) original.getHeight() / sourceImageView.getHeight();

        float x1 = (points.get(0).x) * xRatio;
        float x2 = (points.get(1).x) * xRatio;
        float x3 = (points.get(2).x) * xRatio;
        float x4 = (points.get(3).x) * xRatio;
        float y1 = (points.get(0).y) * yRatio;
        float y2 = (points.get(1).y) * yRatio;
        float y3 = (points.get(2).y) * yRatio;
        float y4 = (points.get(3).y) * yRatio;
        Log.d("", "POints(" + x1 + "," + y1 + ")(" + x2 + "," + y2 + ")(" + x3 + "," + y3 + ")(" + x4 + "," + y4 + ")");

        Bitmap _bitmap = ((ScanActivity) getActivity()).getScannedBitmap(original, x1, y1, x2, y2, x3, y3, x4, y4);
        return _bitmap;

    }

    private class ScanButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            ScanActivity me = (ScanActivity)getActivity();
            if (procBusy) return;
            procBusy = true;


            Map<Integer, PointF> points = polygonView.getPoints();
            if (isScanPointsValid(points)) {


                Uri uri = getUri();
                Integer rotation = getRotation();
                new ScanAsyncTask((ScanActivity) getActivity(), points, uri, rotation).execute();


            } else {

                procBusy = false;
                showErrorDialog();
            }
        }
    }


    private class ScanAsyncTask extends AsyncTask<Void, Void, Uri> {

        ScanActivity mActivity;
        private Map<Integer, PointF> points;
        int rotation;
        Uri uri;

        public ScanAsyncTask(ScanActivity ac, Map<Integer, PointF> points, Uri uri, int rotation) {
            mActivity = ac;
            this.points = points;
            this.uri = uri;
            this.rotation = rotation;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(getString(R.string.scanning));
        }

        @Override
        protected Uri doInBackground(Void... params) {
            try {
                BitmapFactory.Options options = new BitmapFactory.Options();
                AssetFileDescriptor fileDescriptor = null;
                fileDescriptor =
                        mActivity.getContentResolver().openAssetFileDescriptor(uri, "r");

                Bitmap original
                        = BitmapFactory.decodeFileDescriptor(
                        fileDescriptor.getFileDescriptor(), null, options);

                Bitmap bitmapFixed;

                switch (rotation) {
                    case 0:
                    default:
                        bitmapFixed = original;
                        break;
                    case 90:
                        bitmapFixed = rotateImage(original, 90.0f);
                        break;
                    case 180:
                        bitmapFixed = rotateImage(original, 180.0f);
                        break;
                    case 270:
                        bitmapFixed = rotateImage(original, 270.0f);
                        break;
                }

                if (original != bitmapFixed) {
                    original.recycle();
                    original = null;
                }

                Bitmap bm;

                //Utils.logHeap(mActivity);

                bm = getScannedBitmap(bitmapFixed, points);

                bitmapFixed.recycle();
                bitmapFixed = null;


                File folder = Utils.getCacheFolder(mActivity);
                File outputFile = null;
                outputFile = File.createTempFile("tmp", ".jpg", folder);
                FileOutputStream out = null;
                out = new FileOutputStream(outputFile);
                bm.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
                out.close();

                Uri fileUri = Uri.fromFile(outputFile);
                bm.recycle();
                return fileUri;

            } catch (Exception e){
                Log.e(TAG, Utils.getErrorMsg(e));
                return null;
            }
        }

        @Override
        protected void onPostExecute(Uri fileUri) {
            if (getActivity()==null)
                return;

            procBusy = false;
            dismissDialog();
            if(fileUri!=null) {
                scanner.onScanFinish(fileUri);
            } else {
                Toast.makeText(getActivity(),"No se ha  podido recortar la selección", Toast.LENGTH_LONG).show();
            }

        }
    }



    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            ((BitmapDrawable) sourceImageView.getDrawable()).getBitmap().recycle();
        }catch (Exception e){
        }
    }


    @Override
    public void onStop(){
        super.onStop();
    }


    ProgressDialogFragment progressDialogFragment;

    protected  synchronized void showProgressDialog(String message) {
        dismissDialog();
        progressDialogFragment = null;
        progressDialogFragment = new ProgressDialogFragment(message);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        //progressDialogFragment.show(fm, ProgressDialogFragment.class.toString());
        fm.beginTransaction().add(progressDialogFragment,"message")
                .commitAllowingStateLoss();
    }

    protected synchronized void dismissDialog() {

        try {
            progressDialogFragment.dismissAllowingStateLoss();
        }
        catch (Exception e) {
        }
    }



}