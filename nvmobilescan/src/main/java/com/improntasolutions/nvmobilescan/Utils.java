
/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Activity;
import android.app.ActivityManager;
import android.content.Context;
import android.content.CursorLoader;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.net.Uri;
import android.os.Debug;
import android.provider.MediaStore;
import android.text.TextUtils;
import android.util.Log;


import com.itextpdf.text.pdf.PdfWriter;
import  com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Rectangle;


import org.apache.commons.io.FileUtils;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import static android.content.Context.ACTIVITY_SERVICE;

/**
 * Created by jhansi on 05/04/15.
 */
public class Utils {

    private Utils() {

    }

    public static Uri getUri(Context context, Bitmap bitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(context.getContentResolver(), bitmap, "Title", null);
        return Uri.parse(path);
    }

    public static Bitmap getBitmap(Context context, Uri uri) throws IOException {
        Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), uri);
        return bitmap;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        CursorLoader loader = new CursorLoader(context, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }






    public class PDFDocPreference{

        public Integer orientation;
        public Integer size;
        public boolean marginsOn;

    }

    /*public static class A4PDFMargin{
        // en pdf units
        static public float topMargin = 0.75f *72;
        static float leftMargin = 0.75f *72;
    }
    public static class LegalPDFMargin{
        // en pdf units
        static public float topMargin = 0.75f *72;
        static float leftMargin = 0.75f *72;
    }
    public static class PostCardPDFMargin{
        // en pdf units
        static public float topMargin = 0.75f *72;
        static float leftMargin = 0.75f *72;
    }*/



    /*public static byte[] createPDFDocument(Context ctx, ArrayList<String> uris, Integer pdfOri, Integer pdfSize, boolean marginsOn) {

        byte[] pdfDoc = null;

        // Forma 2
        //int image_ppi = 150; // seria ppi! pixels per inch
        int image_ppi = 150;

        // hay que ajustar la resolucion (una reduccion de pixels por pulgada)
        com.itextpdf.text.Document imageDocument = new com.itextpdf.text.Document();
        ByteArrayOutputStream msOutputStream = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(imageDocument, msOutputStream);
        } catch (DocumentException e) {
            e.printStackTrace();
            return null;
        }

        imageDocument.open();

        for (int i = 0; i < uris.size(); i++) {

            try {
                Uri uri = Uri.parse(uris.get(i));

                // leer el tamaño del bitmap pero sin cargar este ultimo en memoria
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                InputStream image_stream = ctx.getContentResolver().openInputStream(uri);
                BitmapFactory.decodeStream(image_stream, null, options);
                image_stream.close();

                // dimensiones del bitmap en pixels
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;


                HashMap<String, Rectangle> pageSizes = new HashMap<String, Rectangle>();
                if(pdfSize==1 || pdfSize==0) {

                    if(pdfOri==0 || pdfOri==1)
                        pageSizes.put("A4", PageSize.A4);

                    if(pdfOri==0 || pdfOri==2)
                        pageSizes.put("A4_LANDSCAPE", new Rectangle(PageSize.A4.getHeight(), PageSize.A4.getWidth(), 0));

                }
                if(pdfSize==2 || pdfSize==0) {
                    if(pdfOri==0 || pdfOri==1)
                        pageSizes.put("LEGAL", PageSize.LEGAL);

                    if(pdfOri==0 || pdfOri==2)
                        pageSizes.put("LEGAL_LANDSCAPE", new Rectangle(PageSize.LEGAL.getHeight(), PageSize.LEGAL.getWidth(), 0));
                }
                if(pdfSize==3 || pdfSize==0) {
                    if(pdfOri==0 || pdfOri==1)
                        pageSizes.put("POSTCARD", PageSize.POSTCARD);

                    if(pdfOri==0 || pdfOri==2)
                        pageSizes.put("POSTCARD_LANDSCAPE", new Rectangle(PageSize.POSTCARD.getHeight(), PageSize.POSTCARD.getWidth(), 0));
                }


                //Calcular que ratio(width/height) de los tipos de hoja se aproxima mas al ratio de la imagen
                float imgRatio = (float) imageWidth / imageHeight;
                Rectangle selected_pageSize = null;
                double min_dif = 1000;
                for (Rectangle pSize : pageSizes.values()){
                    double ratio = pSize.getWidth() * 1.0 / pSize.getHeight();
                    if (Math.abs(ratio - imgRatio) < min_dif) {
                        selected_pageSize = pSize;
                        min_dif = Math.abs(ratio - imgRatio);
                    }
                }

                imageDocument.setPageSize(selected_pageSize);
                if (imageDocument.newPage()) {

                    //if(!marginsOn)
                    //    imageDocument.setMargins(0, 0, 0, 0);


                    float new_width;
                    float new_height;
                    double pageRatio = imageDocument.getPageSize().getWidth() / imageDocument.getPageSize().getHeight();

                    if (pageRatio < imgRatio) {

                        float imageDocumentWidth = (imageDocument.getPageSize().getWidth() / 72) * image_ppi;
                        if (imageDocumentWidth < imageWidth) { //imageDocument.getPageSize().getWidth()
                            new_width = imageDocumentWidth; // 72 puntos/pixels por pulgada en la imagen
                        } else {
                            new_width = imageWidth;
                        }
                        new_height = new_width / imgRatio;

                    } else {

                        float imageDocumentHeight = (imageDocument.getPageSize().getHeight() / 72) * image_ppi;
                        if (imageDocumentHeight < imageHeight) {
                            new_height = imageDocumentHeight;
                        } else {
                            new_height = imageHeight;
                        }
                        new_width = new_height * imgRatio;
                    }

                    // cargar el bitmap con dimensiones multiplo de 2 que mas se aproximan a las dimensiones requeridas
                    // (para evitar cargar un bitmap muy grande en memoria)
                    options.inSampleSize = calculateInSampleSize(options, (int) new_width, (int) new_height);
                    options.inJustDecodeBounds = false;

                    image_stream = ctx.getContentResolver().openInputStream(uri);
                    Bitmap bm0 = BitmapFactory.decodeStream(image_stream, null, options);
                    image_stream.close();

                    // hacer el resize a las dimensiones requeridas
                    Bitmap bm = Bitmap.createScaledBitmap(bm0, (int) new_width, (int) new_height, false);
                    if (bm != bm0) {
                        bm0.recycle();
                    }

                    ByteArrayOutputStream bmp_stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, bmp_stream);
                    bm.recycle();
                    Image img = Image.getInstance(bmp_stream.toByteArray());
                    bmp_stream.close();

                    img.scaleAbsolute((img.getWidth() / image_ppi) * 72, (img.getHeight() / image_ppi) * 72); //escala en user points


                    // margenes sobrantes en x, e y
                    float offx = imageDocument.getPageSize().getWidth() - (new_width / image_ppi) * 72;
                    float offy = imageDocument.getPageSize().getHeight() - (new_height / image_ppi) * 72;

                    offx = (offx>0.f?offx:0);
                    offy = (offy>0.f?offy:0);
                    img.setAbsolutePosition(0, offy);

                    imageDocument.add(img);
                }

            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageDocument.close();
        pdfDoc = msOutputStream.toByteArray();
        try {
            msOutputStream.close();
        } catch (Exception e) {

        }
        return pdfDoc;
    }*/





    public static byte[] createPDFDocument(Context ctx, ArrayList<String> uris, List<PagePreference> pagePreferences, Integer ppi, String depthcolor) {

        byte[] pdfDoc = null;

        if(ppi==null) {
            ppi = 150;
        }

        // hay que ajustar la resolucion (una reduccion de pixels por pulgada)
        com.itextpdf.text.Document imageDocument = new com.itextpdf.text.Document();
        ByteArrayOutputStream msOutputStream = new ByteArrayOutputStream();
        try {
            PdfWriter.getInstance(imageDocument, msOutputStream);
        } catch (DocumentException e) {
            e.printStackTrace();
            return null;
        }

        imageDocument.open();

        for (int i = 0; i < uris.size(); i++) {

            try {
                Uri uri = Uri.parse(uris.get(i));

                // leer el tamaño del bitmap pero sin cargar este ultimo en memoria
                BitmapFactory.Options options = new BitmapFactory.Options();
                options.inJustDecodeBounds = true;
                InputStream image_stream = ctx.getContentResolver().openInputStream(uri);
                BitmapFactory.decodeStream(image_stream, null, options);
                image_stream.close();

                // dimensiones del bitmap en pixels
                int imageHeight = options.outHeight;
                int imageWidth = options.outWidth;


                PagePreference pagePref = pagePreferences.get(i);

                PagePreference.PageOrientation pdfOri = pagePref.pageOrientation;
                PagePreference.PageSize pdfSize = pagePref.pageSize;


                HashMap<String, Rectangle> pageSizes = new HashMap<String, Rectangle>();
                if(pdfSize== PagePreference.PageSize.A4 || pdfSize==PagePreference.PageSize.Auto) {

                    if(pdfOri==PagePreference.PageOrientation.Auto || pdfOri==PagePreference.PageOrientation.Vertical)
                        pageSizes.put("A4", PageSize.A4);

                    if(pdfOri==PagePreference.PageOrientation.Auto || pdfOri==PagePreference.PageOrientation.Horizontal)
                        pageSizes.put("A4_LANDSCAPE", new Rectangle(PageSize.A4.getHeight(), PageSize.A4.getWidth(), 0));

                }
                if(pdfSize==PagePreference.PageSize.Legal || pdfSize==PagePreference.PageSize.Auto) {
                    if(pdfOri==PagePreference.PageOrientation.Auto || pdfOri==PagePreference.PageOrientation.Vertical)
                        pageSizes.put("LEGAL", PageSize.LEGAL);

                    if(pdfOri==PagePreference.PageOrientation.Auto || pdfOri==PagePreference.PageOrientation.Horizontal)
                        pageSizes.put("LEGAL_LANDSCAPE", new Rectangle(PageSize.LEGAL.getHeight(), PageSize.LEGAL.getWidth(), 0));
                }
                if(pdfSize==PagePreference.PageSize.PostCard || pdfSize==PagePreference.PageSize.Auto) {
                    if(pdfOri==PagePreference.PageOrientation.Auto || pdfOri==PagePreference.PageOrientation.Vertical)
                        pageSizes.put("POSTCARD", PageSize.POSTCARD);

                    if(pdfOri==PagePreference.PageOrientation.Auto || pdfOri==PagePreference.PageOrientation.Horizontal)
                        pageSizes.put("POSTCARD_LANDSCAPE", new Rectangle(PageSize.POSTCARD.getHeight(), PageSize.POSTCARD.getWidth(), 0));
                }


                //Calcular que ratio(width/height) de los tipos de hoja se aproxima mas al ratio de la imagen
                float imgRatio = (float) imageWidth / imageHeight;
                Rectangle selected_pageSize = null;
                double min_dif = 1000;
                for (Rectangle pSize : pageSizes.values()){
                    double ratio = pSize.getWidth() * 1.0 / pSize.getHeight();
                    if (Math.abs(ratio - imgRatio) < min_dif) {
                        selected_pageSize = pSize;
                        min_dif = Math.abs(ratio - imgRatio);
                    }
                }

                imageDocument.setPageSize(selected_pageSize);
                if (imageDocument.newPage()) {


                    imageDocument.setMargins(0, 0, 0, 0);

                    float new_width;
                    float new_height;
                    double pageRatio = imageDocument.getPageSize().getWidth() / imageDocument.getPageSize().getHeight();

                    if (pageRatio < imgRatio) {

                        float imageDocumentWidth = (imageDocument.getPageSize().getWidth() / 72) * ppi;
                        if (imageDocumentWidth < imageWidth) { //imageDocument.getPageSize().getWidth()
                            new_width = imageDocumentWidth; // 72 puntos/pixels por pulgada en la imagen
                        } else {
                            new_width = imageWidth;
                        }
                        new_height = new_width / imgRatio;

                    } else {

                        float imageDocumentHeight = (imageDocument.getPageSize().getHeight() / 72) * ppi;
                        if (imageDocumentHeight < imageHeight) {
                            new_height = imageDocumentHeight;
                        } else {
                            new_height = imageHeight;
                        }
                        new_width = new_height * imgRatio;
                    }

                    // cargar el bitmap con dimensiones multiplo de 2 que mas se aproximan a las dimensiones requeridas
                    // (para evitar cargar un bitmap muy grande en memoria)
                    options.inSampleSize = calculateInSampleSize(options, (int) new_width, (int) new_height);
                    options.inJustDecodeBounds = false;

                    image_stream = ctx.getContentResolver().openInputStream(uri);
                    Bitmap bm0 = BitmapFactory.decodeStream(image_stream, null, options);
                    image_stream.close();

                    // hacer el resize a las dimensiones requeridas
                    Bitmap bm = Bitmap.createScaledBitmap(bm0, (int) new_width, (int) new_height, false);
                    if (bm != bm0) {
                        bm0.recycle();
                    }

                    ByteArrayOutputStream bmp_stream = new ByteArrayOutputStream();
                    bm.compress(Bitmap.CompressFormat.JPEG, 100, bmp_stream);
                    bm.recycle();
                    Image img = Image.getInstance(bmp_stream.toByteArray());
                    bmp_stream.close();

                    img.scaleAbsolute((img.getWidth() / ppi) * 72, (img.getHeight() / ppi) * 72); //escala en user points
                    img.setAbsolutePosition(0, imageDocument.getPageSize().getHeight() - (new_height / ppi) * 72);

                    imageDocument.add(img);
                }

            } catch (DocumentException e) {
                e.printStackTrace();
            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        imageDocument.close();
        pdfDoc = msOutputStream.toByteArray();
        try {
            msOutputStream.close();
        } catch (Exception e) {

        }
        return pdfDoc;
    }



    //Calculate the largest inSampleSize value that is a power of 2 and keeps both
    // height and width larger than the requested height and width.
    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // OK: Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }



    public static Bitmap scaledBitmap(Bitmap bitmap, int width, int height) {
        Matrix m = new Matrix();
        m.setRectToRect(new RectF(0, 0, bitmap.getWidth(), bitmap.getHeight()), new RectF(0, 0, width, height), Matrix.ScaleToFit.CENTER);
        return Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), m, true);
    }





    public static Bitmap getThumbBitmap(Context ctx, Uri uri, int width, int height) {

        Bitmap compressed = null;
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;
            InputStream image_stream = null;
            image_stream = ctx.getApplicationContext().getContentResolver().openInputStream(uri);
            BitmapFactory.decodeStream(image_stream, null, options);
            image_stream.close();

            options.inSampleSize = calculateInSampleSize(options, (int) width, (int) height);
            options.inJustDecodeBounds = false;

            image_stream = ctx.getApplicationContext().getContentResolver().openInputStream(uri);
            compressed = BitmapFactory.decodeStream(image_stream, null, options);
            image_stream.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return compressed;
    }




    public static void logHeap(Activity ac) {
        Double allocated = new Double(Debug.getNativeHeapAllocatedSize())/new Double((1048576));
        Double available = new Double(Debug.getNativeHeapSize())/1048576.0;
        Double free = new Double(Debug.getNativeHeapFreeSize())/1048576.0;
        DecimalFormat df = new DecimalFormat();
        df.setMaximumFractionDigits(2);
        df.setMinimumFractionDigits(2);

        Log.d("logHeap", "debug. =================================");
        Log.d("logHeap", "debug.heap native: allocated " + df.format(allocated) + "MB of " + df.format(available) + "MB (" + df.format(free) + "MB free)");
        Log.d("logHeap", "debug.memory: allocated: " + df.format(new Double(Runtime.getRuntime().totalMemory()/1048576)) + "MB of " + df.format(new Double(Runtime.getRuntime().maxMemory()/1048576))+ "MB (" + df.format(new Double(Runtime.getRuntime().freeMemory()/1048576)) +"MB free)");


        ActivityManager am = (ActivityManager) ac.getSystemService(ACTIVITY_SERVICE);
        int memoryClass = am.getMemoryClass();
        int largeMemoryClass = am.getLargeMemoryClass();
        Log.d("logHeap", "memoryClass:" + Integer.toString(memoryClass) + " - largeMemoryClass" + Integer.toString(largeMemoryClass));


    }


    public static String getErrorMsg(Exception e){
        if(e.getMessage()==null){
            return " Error has occurred, but message is not available.";
        } else {
            return e.getMessage();
        }
    }


    public static void clearCacheFolder(Context ctx){
        try {
            File dir = new File(ctx.getExternalCacheDir().getAbsolutePath().concat(File.separator).concat("NvMobileScanCacheDir").concat(File.separator));
            if (dir.exists() && dir.isDirectory()) {
                FileUtils.cleanDirectory(dir);
            }
        } catch (Exception e) {
            Log.e("clearCache","Error deleting 'cache' dir");
        }
    }

    public static File getCacheFolder(Context ctx){

        try {
            File folder =
            new File(ctx.getExternalCacheDir().getAbsolutePath()
                    .concat(File.separator).concat("NvMobileScanCacheDir")
                    .concat(File.separator));

            if (!folder.exists()) {
                folder.mkdirs();
            }

            return folder;
        }catch (Exception e){
            return null;
        }

    }


    public static String getDocEditId(ArrayList<String> urList, ArrayList<String> originalUriList,
                                      ArrayList<PagePreference> pagesPreferences, String fileDesc, Context ctx){

        if(fileDesc==null) fileDesc = "";
        String line1 = TextUtils.join(";", urList);
        String line2 = TextUtils.join(";", originalUriList);
        String line3 = "";
        for (PagePreference ppref : pagesPreferences) {
            line3 += ppref.getPageSize().ordinal() + "," + ppref.getPageOrientation().ordinal() + ";";
        }
        if(line3.length()>0) line3 = line3.substring(0, line3.length()-1);
        String result = line1 + "\n" + line2 + "\n" + line3 + "\n" + fileDesc;

        File folder = Utils.getCacheFolder(ctx);
        File outputFile = null;
        try {
            outputFile = File.createTempFile("idf", "tmp", folder);
        } catch (IOException e) {
            Log.e("Utils.java", e.getMessage());
        }
        FileOutputStream out = null;

        try {
            out = new FileOutputStream(outputFile);
            out.write(result.getBytes());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            try {
                if (out != null) {
                    out.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return  outputFile.getName();
    }


    public static void getDocEditUrls(String docId, ArrayList<String> urlList,
                                      ArrayList<String> originalUriList,
                                      ArrayList<PagePreference> pagesPreferences,
                                      String[] fileDesc,
                                      Context ctx){
        try {
            File folder = Utils.getCacheFolder(ctx);
            File f = new File(folder, docId);

            FileInputStream fin = null;
            fin = new FileInputStream(f);

            BufferedReader reader = new BufferedReader(new InputStreamReader(fin));
            String line = null;

            line = reader.readLine();
            String[] arr = line.split(";");
            for(int i=0;i<arr.length;i++) {
                urlList.add(arr[i]);
            }

            line = reader.readLine();
            arr = line.split(";");
            for(int i=0;i<arr.length;i++) {
                originalUriList.add(arr[i]);
            }

            line = reader.readLine();
            arr = line.split(";");
            for(int i=0;i<arr.length;i++) {
                int pageSize = Integer.parseInt(arr[i].split(",")[0]);
                int pageOrientation = Integer.parseInt(arr[i].split(",")[1]);
                PagePreference pf = new PagePreference();
                pf.setPageOrientation(pageOrientation);
                pf.setPageSize(pageSize);
                pagesPreferences.add(pf);
            }

            line = reader.readLine();
            if(line!=null){
                fileDesc[0] = line;
            }

            reader.close();
            fin.close();
        }catch (Exception e){
            return;
        }
        //return new ArrayList<String>(Arrays.asList(arr));
    }




}