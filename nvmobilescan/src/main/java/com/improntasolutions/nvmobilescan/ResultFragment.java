/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;


public class ResultFragment extends Fragment {
    static final String TAG="ResultFragment";
    PagePreference pagePreference;

    private View view;
    private ImageView scannedImageView;
    private Button doneButton;

    Integer rotationAcum = 0;

    private Button originalButton;
    private Button rotateButton;
    private Button mirrorButton;
    private Button MagicColorButton;
    private Button grayModeButton;
    private Button bwButton;
    private Button btnOrientation;
    private Button btnSize;

    Bitmap compressed;

    enum ImageEffect {
        setOriginal, magickColor
    }

    ArrayList<ImageEffect> effects;

    private IScanner scanner;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof IScanner)) {
            throw new ClassCastException("Activity must implement IScanner");
        }
        this.scanner = (IScanner) activity;
    }

    public ResultFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.result_layout, null);
        init(savedInstanceState);
        return view;
    }


    boolean procBusy = false;
    FrameLayout sourceFrame;
    private void init( Bundle savedInstanceState) {

        scannedImageView = (ImageView) view.findViewById(R.id.scannedImage);
        originalButton = (Button) view.findViewById(R.id.original);
        originalButton.setOnClickListener(new OriginalButtonClickListener());
        rotateButton = (Button) view.findViewById(R.id.rotateButton);
        rotateButton.setOnClickListener(new rotateButtonClickListener());
        mirrorButton = (Button) view.findViewById(R.id.mirrorButton);
        mirrorButton.setOnClickListener(new mirrorButtonClickListener());

        MagicColorButton = (Button) view.findViewById(R.id.magicColor);
        MagicColorButton.setOnClickListener(new MagicColorButtonClickListener());
        /*grayModeButton = (Button) view.findViewById(R.id.grayMode);
        grayModeButton.setOnClickListener(new GrayButtonClickListener());
        bwButton = (Button) view.findViewById(R.id.BWMode);
        bwButton.setOnClickListener(new BWButtonClickListener());*/


        sourceFrame = (FrameLayout) view.findViewById(R.id.srcFrame);
        doneButton = (Button) view.findViewById(R.id.doneButton);


        btnOrientation = view.findViewById(R.id.btnOrientation);
        btnSize = view.findViewById(R.id.btnSize);

        final ResultFragment me = this;
        btnOrientation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PageOrientationPrefDialog frag = new PageOrientationPrefDialog();
                Bundle args = new Bundle();
                args.putSerializable("title", "Page 1");
                frag.setArguments(args);
                frag.setTargetFragment(me, 0);
                getFragmentManager().beginTransaction().add(frag, "message")
                        .commitAllowingStateLoss();

                //frag.show(getFragmentManager(), "dialog");
            }
        });


        btnSize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final PageSizePrefDialog frag = new PageSizePrefDialog();
                Bundle args = new Bundle();
                args.putSerializable("title", "Page 1");
                frag.setArguments(args);
                frag.setTargetFragment(me, 0);
                getFragmentManager().beginTransaction().add(frag, "message")
                        .commitAllowingStateLoss();
            }
        });


        if (savedInstanceState != null) {
            pagePreference = savedInstanceState.getParcelable("page_preference");
        } else {
            if (getUriIndex() > -1) {
                pagePreference = ((ScanActivity) getActivity()).pagesPreferences.get(getUriIndex());
            } else {
                pagePreference = new PagePreference();
                // leer los valores por defaul
                SharedPreferences settings = PreferenceManager.getDefaultSharedPreferences(getActivity());
                final Integer pdfOri = Integer.parseInt(settings.getString("key_pdf_orientation", "0"));
                final Integer pdfSize = Integer.parseInt(settings.getString("key_pdf_size", "0"));
                final boolean marginsOn = settings.getBoolean("key_pdf_margin", false);
                pagePreference.setPageOrientation(pdfOri);
                pagePreference.setPageSize(pdfSize);
                doneButton.setText("AGREGAR"); // por defecto el string es igual a "LISTO"
            }
        }

        doneButton.setEnabled(false);
        ;
        originalButton.setEnabled(true);
        rotateButton.setEnabled(true);
        MagicColorButton.setEnabled(true);

        effects = new ArrayList<ImageEffect>();

        final Uri uri = getArguments().getParcelable(ScanConstants.SCANNED_RESULT);


        sourceFrame.post(new Runnable() {
            @Override
            public void run() {

                try {
                    //original = BitmapFactory.decodeFile(uri.getEncodedPath());
                    // Comprimiendo asi: mucha ram
                    // compressed = Utils.scaledBitmap(original, sourceFrame.getWidth(), sourceFrame.getHeight());
                    compressed = Utils.getThumbBitmap(getActivity(), getUri(), sourceFrame.getWidth(), sourceFrame.getHeight());
                    scannedImageView.setImageBitmap(compressed);
                    doneButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ScanActivity me = (ScanActivity) getActivity();
                            if (procBusy) return;
                            procBusy = true;

                            // si esta en modo edicion, y no hizo cambios, salir sin hacer nada
                            if (getUriIndex() > -1 && effects.size() == 0 && rotationAcum == 0) {
                                scanner.showResult();
                                return;
                            } else {
                                new ResultTask(me, getUri(), getUriIndex(), pagePreference).execute();
                            }
                        }
                    });
                    doneButton.setEnabled(true);
                    originalButton.setEnabled(true);
                    rotateButton.setEnabled(true);
                    MagicColorButton.setEnabled(true);

                } catch (Exception e) {
                    // probables errores de nullpointer cuando la actividad es nula
                    Log.e(TAG, Utils.getErrorMsg(e));
                }
            }
        });
    }



    private Uri getUri() {
        Uri uri = getArguments().getParcelable(ScanConstants.SCANNED_RESULT);
        return uri;
    }

    private Integer getUriIndex(){
        Integer uriIndex = getArguments().getInt("uri_index", -1);
        return uriIndex;
    }


    private class rotateButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            // create a matrix object
            Matrix matrix = new Matrix();
            matrix.postRotate(90.0f); // anti-clockwise by 90 degrees

            Bitmap out =  Bitmap.createBitmap(compressed , 0, 0, compressed.getWidth(), compressed.getHeight(), matrix,true);
            if(out!=compressed){
                compressed.recycle();
            }
            compressed = out;
            scannedImageView.setImageBitmap(compressed);
            rotationAcum = (rotationAcum + 90) % 360;
        }
    }

    private class mirrorButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // create a matrix object
            Matrix matrix = new Matrix();
            matrix.postScale(-1,1); // anti-clockwise by 90 degrees
        }
    }



/*
    private class BWButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = ((ScanActivity) getActivity()).getBWBitmap(original);
            scannedImageView.setImageBitmap(transformed);
        }
    }
/*
    private class MagicColorButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = ((ScanActivity) getActivity()).getMagicColorBitmap(original);
            scannedImageView.setImageBitmap(transformed);
        }
    }



    private class GrayButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            transformed = ((ScanActivity) getActivity()).getGrayBitmap(original);
            scannedImageView.setImageBitmap(transformed);
        }
    }
*/

    private class OriginalButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            new Thread(new Runnable() {
                @Override
                public void run() {

                    effects.clear();
                    rotationAcum = 0;

                    if (getUriIndex() > -1) {

                        // arranca de la version base
                        effects.add(ImageEffect.setOriginal);

                        compressed.recycle();
                        String uri = ((ScanActivity) getActivity()).originalUriList.get(getUriIndex());
                        compressed = Utils.getThumbBitmap(getActivity(), Uri.parse(uri), sourceFrame.getWidth(), sourceFrame.getHeight());

                    } else {

                        compressed.recycle();
                        compressed = Utils.getThumbBitmap(getActivity(), getUri(), sourceFrame.getWidth(), sourceFrame.getHeight());

                    }

                    //nPassFilters = 0;
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {

                            scannedImageView.setImageBitmap(compressed);
                        }
                    });
                }
            }).start();
        }
    }


    private class MagicColorButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            final ScanActivity me = (ScanActivity) getActivity();
            //showProgressDialog("Aplicando filtro...");

            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Bitmap out = ((ScanActivity) getActivity()).getMagicColorBitmap(compressed);
                    if(out!=compressed){
                        compressed.recycle();
                    }
                    compressed = out;

                    effects.add(ImageEffect.magickColor);

                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            scannedImageView.setImageBitmap(compressed);

                        }
                    });
                }
            }).start();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if(pagePreference!=null)
            outState.putParcelable("page_preference",pagePreference);
    }


    public class ResultTask extends AsyncTask<Void, Void,Boolean> {


        ScanActivity mActivity;
        int uriIndex;
        Uri bitmapUri;
        PagePreference pagePreference;

        public ResultTask(ScanActivity mainActivity,  Uri bitmapUri, int uriIndex, PagePreference pagePreference) {
            this.mActivity = mainActivity;
            this.bitmapUri = bitmapUri;
            this.uriIndex= uriIndex;
            this.pagePreference = pagePreference;
        }

        @Override
        protected void onPreExecute() {
            showProgressDialog(getString(R.string.on_result_image_message));
        }

        @Override
        protected Boolean doInBackground(Void... params) {
            try {

                System.gc();

                //cargar la version base o la traida
                boolean loadOriginal = false;
                if(effects.size()>0){
                    if(effects.get(0) ==ImageEffect.setOriginal){
                        loadOriginal = true;
                        effects.remove(0);
                    }
                }
                Bitmap bitmap = null;
                if(!loadOriginal) {
                    bitmap = BitmapFactory.decodeFile(bitmapUri.getEncodedPath());
                }
                else {
                    String uri = mActivity.originalUriList.get(getUriIndex());
                    bitmap = BitmapFactory.decodeFile(Uri.parse(uri).getEncodedPath());
                }


                Bitmap outbitmap = null;
                if(rotationAcum!=0) {
                    Matrix matrix = new Matrix();
                    matrix.postRotate(rotationAcum); // anti-clockwise by 90 degrees
                    outbitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
                }


                if(outbitmap==null){
                    outbitmap = bitmap;
                } else {
                    bitmap.recycle();
                }
                System.gc();


                Bitmap outbitmap2 = null;
                Bitmap finalBitmap = null;


                for(int i=0;i<effects.size();i++){

                    ImageEffect effect = effects.get(i);
                    if(effect==ImageEffect.magickColor){
                        if (i % 2 == 0) {
                            outbitmap2 = mActivity.getMagicColorBitmap(outbitmap);
                            outbitmap.recycle();
                            finalBitmap = outbitmap2;
                        } else {
                            outbitmap = mActivity.getMagicColorBitmap(outbitmap2);
                            outbitmap2.recycle();
                            finalBitmap = outbitmap;
                        }
                    }

                }

                if(finalBitmap==null){
                    finalBitmap = outbitmap;
                }

                File folder = Utils.getCacheFolder(mActivity);
                File outputFile = null;
                outputFile = File.createTempFile("tmp", ".jpg", folder);
                FileOutputStream out = null;
                out = new FileOutputStream(outputFile);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 100, out); // bmp is your Bitmap instance
                out.close();

                Uri uri = Uri.fromFile(outputFile);
                finalBitmap.recycle();

                System.gc();

                if (uriIndex > -1) {
                    mActivity.uriList.set(uriIndex, uri.toString());
                    mActivity.pagesPreferences.set(uriIndex, pagePreference);
                }
                else {
                    mActivity.uriList.add(uri.toString());
                    mActivity.originalUriList.add(bitmapUri.toString()); // guardar la url original de la imagen sin retocar
                    mActivity.pagesPreferences.add(pagePreference); // guardar la url original de la imagen sin retocar
                }
                return true;
            } catch (Exception e) {
                Log.e(TAG, Utils.getErrorMsg(e));
                return false;
            }
        }

        @Override
        protected void onPostExecute(Boolean result) {

            if (getActivity() != null) {

                dismissDialog();
                procBusy = false;
                if (result) {
                    scanner.showResult();
                } else {
                    Toast.makeText(getActivity(), "No se pudo generar la imagen", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    ProgressDialogFragment progressDialogFragment;

    void showProgressDialog(String message) {
        dismissDialog();
        progressDialogFragment = new ProgressDialogFragment(message);
        FragmentManager fm = getActivity().getSupportFragmentManager();
        //progressDialogFragment.show(fm, ProgressDialogFragment.class.toString());
        fm.beginTransaction().add(progressDialogFragment,"message")
                .commitAllowingStateLoss();
    }

    void dismissDialog() {
        try {
            progressDialogFragment.dismissAllowingStateLoss();
        }
        catch (Exception e) {
        }
    }


    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            if (compressed != null) {
                compressed.recycle();
            }
        }catch(Exception e){
        }
        try {
            ((BitmapDrawable) scannedImageView.getDrawable()).getBitmap().recycle();
        }catch (Exception e){
        }
    }


}