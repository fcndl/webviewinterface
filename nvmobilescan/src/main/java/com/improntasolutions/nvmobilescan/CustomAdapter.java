/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by flezcano on 22/12/2016.
 */

public class CustomAdapter extends BaseAdapter {

    Context context;
    List<ImageItem> menuItems;
    Boolean checkMode = false;


    View.OnDragListener onDragListener;

    CustomAdapter(Context context, List<ImageItem> menuItems) {
        this.context = context;
        this.menuItems = menuItems;
    }


    @Override
    public int getCount() {
        return menuItems.size();
    }

    public ImageItem getImageItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public Object getItem(int position) {
        return menuItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return menuItems.indexOf(getItem(position));
    }


    private class ViewHolder {
        ImageView ivMenu;
        TextView tvMenuHeader;
    }

    public void setItemOnDragListener(View.OnDragListener odl){
        this.onDragListener = odl;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder holder = null;

        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.image_item, null);
            holder = new ViewHolder();

            holder.tvMenuHeader = (TextView) convertView.findViewById(R.id.tvMenuHeader);
            holder.ivMenu = (ImageView) convertView.findViewById(R.id.ivMenuItem);

            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        ImageItem row_pos = menuItems.get(position);

        /*Picasso.with(context)
                .load(new File(row_pos.getItem_image_url()))//.load("http://novatest.improntasolutions.com/captcha/captcha.ashx?id=MHDNDUKHJSZTICRN")//.load(row_pos.getItem_image_url())//
                .into(holder.ivMenu);*/

        //Picasso.with(context).load(row_pos.getItem_image_url()).fit().centerInside().into(holder.ivMenu);




        /*Picasso picasso = new Picasso.Builder(context).listener(
                new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri,
                                                  Exception exception) {
                        exception.printStackTrace();
                    } }).debugging(true).build();
        picasso.with(context).load(row_pos.getItem_image_url()).fit().centerInside().into(holder.ivMenu);*/






        /*Picasso picasso = new Picasso.Builder(context).listener(
                new Picasso.Listener() {
                    @Override
                    public void onImageLoadFailed(Picasso picasso, Uri uri,
                                                  Exception exception) {
                        exception.printStackTrace();
                    } }).debugging(true).build();

        picasso.with(context).load(row_pos.getItem_image_url()).fit().centerCrop().into(holder.ivMenu);*/



        Picasso.with(context)
                .load(row_pos.getItem_image_url()).skipMemoryCache()
                .fit()
                .centerCrop()
                .into(holder.ivMenu);







        //picasso.load(row_pos.getItem_image_url()).resize(200, 200).into(holder.ivMenu);
        //final String row_url  = row_pos.getItem_image_url();

        /*new Thread() {
            @Override public void run() {
                Bitmap bitmap;
                try {
                    bitmap = Picasso.with(context).load(new File(row_url)).get();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.start();*/

        holder.tvMenuHeader.setText(row_pos.getItem_header());

        Log.e("Test", "headers:" + row_pos.getItem_header());


        if(checkMode) {
            ((CheckableLayout)(convertView)).setChecked(row_pos.isSelected());
            ((CheckableLayout)(convertView)).showCheck();
            ((CheckableLayout)(convertView)).setOnDragListener(onDragListener);

            //((CheckBox) ((ViewGroup) convertView).getChildAt(2)).setVisibility(VISIBLE);
        }
        else {
            ((CheckableLayout)(convertView)).setChecked(false);
            ((CheckableLayout)(convertView)).hideCheck();
            ((CheckableLayout)(convertView)).setOnDragListener(null);
            //((CheckBox) ((ViewGroup) convertView).getChildAt(2)).setVisibility(View.GONE);
        }


        return convertView;
    }

    /*public String getAbsolutePathFromURI( String uri ) {
        Uri contentUri = new URL(uri);
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query( contentUri, proj, null, null, null );
        int columnIndex = cursor.getColumnIndexOrThrow( MediaStore.Images.Media.DATA );
        cursor.moveToFirst();
        String path = cursor.getString( columnIndex );
        cursor.close();
        return path;
    }*/



    public void setCheckMode(boolean val){
        this.checkMode = val;
    }


    public int getSelectedCount(){

        int count = 0;
        for (int i=0;i<menuItems.size();i++){
            if(menuItems.get(i).isSelected()){
                count++;
            }
        }
        return count;

    }



    public void clearSelecteds(){
        for (int i=0;i<menuItems.size();i++){
            menuItems.get(i).setSelected(false);
        }
    }



    public List<Integer> getSelectedIndexes(){

        List<Integer> indexes = new ArrayList<Integer>();
        for(int i=0;i< menuItems.size();i++){
            if(menuItems.get(i).isSelected()){
                indexes.add(i);
            }
        }
        return indexes;
    }












}