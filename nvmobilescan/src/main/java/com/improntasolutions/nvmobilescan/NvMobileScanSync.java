/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.webkit.JavascriptInterface;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by flezcano on 03/04/2018.
 */


/**
 * Clase que permite editar imagenes tomadas desde la cámara o desde el file explorer y convertirlas
 * a documento pdf. Ofrece auto recuadro para el recorte de imagen, y funciones para la rotación y mejora de contraste.
 * Además permite establecer la orientacion del pdf de salida, asi como tambien su tamaño:
 * a4, legal, o postcard
 */
public class NvMobileScanSync {

    //private static String MY_PREFS_NAME = "NvMobileScanPreferences";
    Activity ac;
    Runnable rr;

    /**
     * Números de RequestCode con el que se lanzan las actividades
     * para captura de imagen: cámara o file explorer
     */
    public int CAPTURE_CODE = 1555;
    public int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1560;

    /**
     * Número de permissionRequestCode con el que se lanza la solicitud
     * de permisos para el uso de la clase. Se solicita acceso a cámara,
     * y acceso a almacenamiento externo.
     */
    public int CAMERA_PERMISSION_REQUEST_CODE = 1561;


    // parametros scan pdf
    Integer ppi;
    String fileDesc;
    Integer default_ppi;
    String default_path;
    String path;



    // archivo salida
    Uri fileUri;
    String fileName;
    String docString;
    byte[] docByteArray;

    //boolean pathOutput; //indica si se esta ejecutando metodo que devuelve el path del pdf como output

    /**
     * Constructor de la clase
     *
     * @param ac Actividad en el que se declara la instancia de clase NvMobileScan
     */
    public NvMobileScanSync(Activity ac) {
        this.ac = ac;

        //SharedPreferences prefs = ac.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE);
        //default_ppi = prefs.getInt("default_ppi", 120);
        default_ppi = 120;

        String path_internal_storage = ac.getFilesDir().getAbsolutePath();
        //default_path = prefs.getString("default_path",path_internal_storage);
        default_path = path_internal_storage;
    }

    /**
     * Setea los puntos por pulgada(ppi) por defecto utilizados para generar los archivos pdf. Si no
     * se especifica toma el valor preestablecido de 120
     *
     * @param defaultDpi nuevo valor ppi por defecto que se desea establecer
     */
    public void setDefault_ppi(Integer defaultDpi) {
        this.default_ppi = defaultDpi;
        //SharedPreferences.Editor editor = ac.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        //editor.putInt("default_ppi", defaultDpi);
        //editor.commit();
    }

    /**
     * Obtener los ppi por defecto establecidos
     *
     * @return valor de ppi por defecto
     */
    public Integer getDefault_ppi() {
        return default_ppi;
    }

    /**
     * Establece el path por defecto donde se guardaran los pdf generados.
     * Si no se establece, los pdf generados se guardaran en la cache externa de la app
     * con nombres generados a partir de timestamp
     *
     * @param default_path path por defecto donde guardar los archivos pdf generados
     */
    public void setDefault_path(String default_path) {
        this.default_path = default_path;
        //SharedPreferences.Editor editor = ac.getSharedPreferences(MY_PREFS_NAME, MODE_PRIVATE).edit();
        //editor.putString("default_path", default_path);
        //editor.commit();
    }

    /**
     * Obtener el path por defecto donde se gaurdan los pdf generados
     *
     * @return path por defecto donde se gaurdan los pdf generados
     */
    public String getDefault_path() {
        return default_path;
    }


    private void callEdit() {

        if (PermissionsUtils.checkPermissionForExternalStorage(ac)) {
            String depthcolor = "truecolor";
            int REQUEST_CODE = CAPTURE_CODE;
            int preference = ScanConstants.OPEN_MEDIA;
            Intent intent = new Intent(ac, ScanActivity.class);
            intent.putExtra("fileDesc", fileDesc);
            intent.putExtra("ppi", ppi);
            intent.putExtra("default_ppi", default_ppi);
            intent.putExtra("path", path);
            intent.putExtra("default_path", default_path);
            intent.putExtra("depthcolor", depthcolor);
            intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            ac.startActivityForResult(intent, REQUEST_CODE);
        } else {
            PermissionsUtils.requestPermissionForExternalStorage(ac, EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }


    private void callScan() {
        String depthcolor = "truecolor";
        if (PermissionsUtils.checkPermissionForCamera(ac)) {
            int REQUEST_CODE = CAPTURE_CODE;
            int preference = ScanConstants.OPEN_CAMERA;
            Intent intent = new Intent(ac, ScanActivity.class);
            intent.putExtra("fileDesc", fileDesc);
            intent.putExtra("ppi", ppi);
            intent.putExtra("default_ppi", default_ppi);
            intent.putExtra("path", path);
            intent.putExtra("default_path", default_path);
            intent.putExtra("depthcolor", depthcolor);
            intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            ac.startActivityForResult(intent, REQUEST_CODE);
        } else {
            PermissionsUtils.requestPermissionForCamera(ac, CAMERA_PERMISSION_REQUEST_CODE);
        }
    }



    /*
    // devuelven paths
    public String editDocumentFromScan(Integer inputPpi, String path, String inputFileDesc) {
        String strOutput = getBlobFromEditDocument( inputPpi,  path,  inputFileDesc);
        if(strOutput==null){
            return null;
        }
        return pdfUri.getEncodedPath();
    }

    public String editDocumentFromScan() {
        String strOutput = getBlobFromEditDocument( null,  null,  null);
        if(strOutput==null){
            return null;
        }
        return pdfUri.getEncodedPath();
    }*/

    /**
     * Lanza la ejecución de la actividad de edición de imagen a partir del explorador
     * de archivos.
     *
     * @return string en base64 del binario del archivo pdf generado,o null si se cancela la operación.
     */
    public String editDocumentFromScan() {
        return getBlobFromEditDocument(null, null, null);
    }


    /**
     * Lanza la ejecución de la actividad de edición de imagen a partir del explorador
     * de archivos.
     *
     * @param inputPpi      la calidad en ppi que se desean para el archivo pdf
     * @param path          el path completo del archivo de salida pdf
     * @param inputFileDesc descripción del documento que se está por editar. Sirve para mostrarla
     *                      en la pantalla de edición de imagenes.
     * @return string en base64 del binario del archivo pdf generado,o null si se cancela la operación.
     */
    public String editDocumentFromScan(Integer inputPpi, String path, String inputFileDesc) {
        return getBlobFromEditDocument(inputPpi, path, inputFileDesc);
    }


    public String getBlobFromEditDocument(Integer inputPpi, String path, String inputFileDesc) {

        this.ppi = inputPpi;
        this.fileDesc = inputFileDesc;
        this.path = path;

        rr = new Runnable() {
            @Override
            public void run() {
                callEdit();
            }
        };

        try {
            synchronized (rr) {
                ac.runOnUiThread(rr);
                rr.wait(); // unlocks myRunable while waiting
            }
        } catch (Exception e) {
            return null;
        }

        if (docString == null)
            return null;

        //return "data:application/pdf;base64," + docString;
        return docString;
    }


    /*public String getDocumentFromScan(Integer inputPpi, String path, String inputFileDesc )  {
        String strOutput = getBlobFromScanDocument( inputPpi,  path,  inputFileDesc);
        if(strOutput==null){
            return null;
        }
        return pdfUri.getEncodedPath();
    }

    public String getDocumentFromScan()  {
        String strOutput = getBlobFromScanDocument( null,  null,  null);
        if(strOutput==null){
            return null;
        }
        return pdfUri.getEncodedPath();
    }*/

    /**
     * Lanza la ejecución de la actividad de edición de imagen a partir de la captura por cámara.
     *
     * @return string en base64 del binario del archivo pdf generado,o null si se cancela la operación.
     */
    public Map<String,Object> getDocumentFromScan() {
        return getBlobFromScanDocument(null, null, null);
    }

    /**
     * Lanza la ejecución de la actividad de edición de imagen a partir de la captura por cámara.
     *
     * @param inputPpi      la calidad en ppi que se desean para el archivo pdf
     * @param path          el path completo del archivo de salida pdf
     * @param inputFileDesc descripción del documento que se está por editar. Sirve para mostrarla
     *                      en la pantalla de edición de imagenes.
     * @return string en base64 del binario del archivo pdf generado,o null si se cancela la operación.
     */
    public  Map<String,Object>  getDocumentFromScan(Integer inputPpi, String path, String inputFileDesc) {
        return getBlobFromScanDocument(inputPpi, path, inputFileDesc);
    }

    public Map<String,Object> getBlobFromScanDocument(Integer inputPpi, String path, String inputFileDesc) {

        this.ppi = inputPpi;
        this.fileDesc = inputFileDesc;
        this.path = path;

        rr = new Runnable() {
            @Override
            public void run() {
                callScan();
            }
        };

        try {
            synchronized (rr) {
                ac.runOnUiThread(rr);

                rr.wait(); // unlocks myRunable while waiting
            }
        } catch (Exception e) {
            return null;
        }

        if (docString == null)
            return null;

        //return "data:application/pdf;base64," + docString;
        //return docString;


        //return "{ \"b64str\": \"" + docString + "\", \"filename\": \"" + fileName + "\" }";
        HashMap<String, Object> res  = new HashMap<String, Object>();
        res.put("fileBytes",docByteArray);
        res.put("fileName", fileName);
        return res;
    }


    /**
     * Método que implementa el evento onActivityResult para la captura de resultados para
     * las actividades de edición de imagen por camara y file explorer.
     * Debe ser llamada obligatoriamente desde el callback onActivityResult
     * de la actividad en la que se instancio el objeto de clase NvMobileScan.
     *
     * @param requestCode variable requestCode arrojada por el callback onActivityResult de la actividad host
     *                    del objeto de de clase NvMobileScan.
     * @param resultCode  variable resultCode arrojada por el callback onActivityResult de la actividad host
     *                    del objeto de de clase NvMobileScan.
     * @param data        variable data arrojada por el callback onActivityResult de la actividad host
     *                    del objeto de de clase NvMobileScan.
     */

    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == CAPTURE_CODE && resultCode == RESULT_OK) {
            fileUri = data.getParcelableExtra("pdf_uri");

            if (fileUri == null) {
                failureUnlock();
                return;
            }

            fileName = getFileName(fileUri);

            new Thread(new Runnable() {
                @Override
                public void run() {

                    final byte[] docBytes;
                    try {
                        //docBytes = FileUtils.readFileToByteArray(new File(fileUri.getEncodedPath()));
                        docBytes = IOUtils.toByteArray( ac.getContentResolver().openInputStream(fileUri));

                        String base64String = Base64.encodeToString(docBytes, Base64.NO_WRAP);
                        docString = base64String;
                        docByteArray = docBytes;

                        synchronized (rr) {
                            rr.notify();
                            rr = null;
                        }

                    } catch (Exception e) {
                        //e.printStackTrace();
                        failureUnlock();
                    }
                }
            }).start();


        } else {
            // por si sale sin resultado, hay que desbloquear
            failureUnlock();
        }

    }


    private void failureUnlock() {
        try {
            synchronized (rr) {
                rr.notify();
            }
        } catch (Exception e) {
        } finally {
            rr = null;
            docString = null;
            docByteArray = null;
        }
    }


    /**
     * Método para liberar la instancia de NvMobileScan. Se debe
     * llamar obligatoriamente dentro del onDestroy de la actividad host
     */
    public void release() {
        try {
            synchronized (rr) {
                rr.notify();
            }
        } catch (Exception e) {
        } finally {
            rr = null;
            docString = null;
            docByteArray = null;
        }
    }



    private String getFileName(Uri uri) {
        String displayName = null;
        try {
            String uriString = uri.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = ac.getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
            }
        } catch (Exception e) {
        }
        return displayName;
    }


    /**
     * Método que implementa el evento onRequestPermissionsResult para el resultado de la solicitud
     * permisos de cámara y acceso a almacenamiento externo que necesita la clase NvMobileScan. Debe
     * ser llamado obligatoriamente desde el callback onRequestPermissionsResult de la actividad host
     * del objeto de clase NvMobileScan
     *
     * @param requestCode  variable requestCode que arroja el callback onRequestPermissionsResult
     *                     de la actividad host
     *                     del objeto de clase NvMobileScan
     * @param permissions  variable permissions que arroja el callback onRequestPermissionsResult
     *                     de la actividad host
     *                     el objeto de clase NvMobileScan
     * @param grantResults variable grantResults que arroja el callback onRequestPermissionsResult
     *                     de la actividad host
     *                     el objeto de clase NvMobileScan
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    callScan();
                    return;

                } else {

                    // soltar el proceso sincronico
                    release();

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.CAMERA) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay

                    } else {

                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(ac, false);
                    }
                }
            }
        }

        if (requestCode == EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    callEdit();
                    return;

                } else {

                    // soltar el proceso sincronico
                    release();

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay

                    } else {
                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(ac, false);
                    }

                }
            }
        }
    }


}
