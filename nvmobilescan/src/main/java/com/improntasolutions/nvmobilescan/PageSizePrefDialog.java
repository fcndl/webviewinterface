/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.RadioGroup;
import android.widget.TextView;



/**
 * Created by flezcano on 27/11/2017.
 */



public class PageSizePrefDialog extends DialogFragment {

    String title;

    public PageSizePrefDialog() {
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        this.title = (String) args.getSerializable("title");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);

        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(true);
        return dialog;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.page_size_pref, container, false);
        TextView tvTitle = root.findViewById(R.id.title);

        if(savedInstanceState!=null){
            String text_title = savedInstanceState.getString("title","");
            this.title = text_title;
        }

        tvTitle.setText(title);

        RadioGroup rGroup = root.findViewById(R.id.rgroup);
        PagePreference.PageSize pageSize = ((ResultFragment) getTargetFragment()).pagePreference.getPageSize();
        switch (pageSize){
            case Auto:
                rGroup.check(R.id.rbAuto);
                break;
            case A4:
                rGroup.check(R.id.rbA4);
                break;
            case Legal:
                rGroup.check(R.id.rbLegal);
                break;
            case PostCard:
                rGroup.check(R.id.rbPostcard);
                break;
        }


        rGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {

                if (checkedId == R.id.rbAuto) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageSize(PagePreference.PageSize.Auto);
                }
                if (checkedId == R.id.rbA4) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageSize(PagePreference.PageSize.A4);
                }
                if (checkedId == R.id.rbLegal) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageSize(PagePreference.PageSize.Legal);
                }
                if (checkedId == R.id.rbPostcard) {
                    ((ResultFragment) getTargetFragment()).pagePreference.setPageSize(PagePreference.PageSize.PostCard);
                }
                dismiss();
            }
        });


        return root;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onSaveInstanceState(Bundle b){
        b.putString("title", title);
    }

}
