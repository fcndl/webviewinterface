/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

public class SyncFileExplorer extends AppCompatActivity {
    static final Integer PICK_FILE = 1378;
    static final Integer EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1784;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.activity_sync_file_explorer);

        callFileExplorer();

    }


    public void callFileExplorer(){

        if(PermissionsUtils.checkPermissionForExternalStorage(this)) {
            Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            intent.setType("*/*");
            //intent.setType("application/pdf");

            try {
                startActivityForResult(Intent.createChooser(intent, "Seleccione su legajo"), PICK_FILE);
            } catch (android.content.ActivityNotFoundException ex) {
                Toast.makeText(this, "Por favor, instale un explorador de archivos", Toast.LENGTH_SHORT).show();
                finish();
            }

        } else {
            PermissionsUtils.requestPermissionForExternalStorage(this,EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==PICK_FILE && resultCode==Activity.RESULT_OK){
            Uri file_path = data.getData();
            Intent intent = new Intent();
            intent.setData(file_path);
            intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            intent.putExtra("pdf_uri", file_path);
            setResult(Activity.RESULT_OK, intent);
        }
        finish();

    }




    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    callFileExplorer();
                    return;

                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay
                        finish();
                        return;
                    } else {
                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(this, true);
                        return;
                    }
                }
            }
        }

    }


}



