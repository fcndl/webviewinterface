/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.OpenableColumns;
import android.util.Base64;
import org.apache.commons.io.IOUtils;
import java.io.File;

import static android.app.Activity.RESULT_OK;

/**
 * Created by flezcano on 10/04/2018.
 */

public class NvMobileScanChooser {

    Activity ac;
    Runnable rr;

    // parametros scan pdf
    Integer default_ppi;
    String default_path;

    // parametro
    public boolean clearCacheFolderOnInit;

    // archivo salida
    Uri fileUri;
    String docEditId = "";
    String fileName;
    String docString;
    byte[] docByteArray;

    public int CREATE_PDF_REQUEST_CODE = 1561;


    public NvMobileScanChooser(Activity ac){
        this.ac = ac;
        default_ppi = 120;
        String path_internal_storage = ac.getFilesDir().getAbsolutePath();
        default_path = path_internal_storage;
        clearCacheFolderOnInit = true;
    }


    public void setDefault_ppi(Integer defaultDpi){
        this.default_ppi = defaultDpi;
    }


    public Integer getDefault_ppi(){
        return  default_ppi;
    }


    public void setDefault_path(String default_path) {
        this.default_path = default_path;
    }


    public String getDefault_path(){
        return  default_path;
    }


    public String getDocumentFromChooser(){
        this.docEditId = "";
        return startChooser(null, null, null);
    }


    public String getDocumentFromChooser(Integer inputPpi, String path, String inputFileDesc )  {
        this.docEditId = "";
        return startChooser(inputPpi, path, inputFileDesc);
    }


    public String getDocumentFromChooser(String inputFileDesc )  {
        this.docEditId = "";
        return startChooser(null, null, inputFileDesc);
    }


    public String editScannedDocument(String docEditId){
        this.docEditId = docEditId;
        return startChooser(null, null, null);
    }


    public String startChooser(final Integer inputPpi, final String path, final String inputFileDesc)  {

        rr = new Runnable() {
            @Override
            public void run() {
                callChooser(inputPpi, path, inputFileDesc);
            }
        };

        try {
            synchronized (rr) {
                ac.runOnUiThread(rr);
                rr.wait(); // unlocks myRunable while waiting
            }
        }catch (Exception e){
            return null;
        }

        if(docString==null)
            return null;

        return "{ \"b64str\": \"" + docString + "\", \"filename\": \"" + fileName + "\", \"docEditId\": \"" + docEditId + "\"}";
    }


    public void clearCacheFolder(){
        new Thread(new Runnable() {
            @Override
            public void run() {
                Utils.clearCacheFolder(ac);
            }
        }).start();
    }


    private void callChooser(Integer inputPpi, String path, String inputFileDesc) {
        Intent intentCamera = new Intent(ac, CameraScanWrapperActivity.class);
        intentCamera.putExtra("fileDesc", inputFileDesc);
        intentCamera.putExtra("ppi", inputPpi);
        intentCamera.putExtra("default_ppi", default_ppi);
        intentCamera.putExtra("path", path);
        intentCamera.putExtra("default_path", default_path);
        intentCamera.putExtra("depthcolor", "truecolor");
        intentCamera.putExtra("clearCacheFolderOnInit", clearCacheFolderOnInit);
        intentCamera.putExtra("doc_edit_id", docEditId);

        if(docEditId!= null && !docEditId.isEmpty()) {
            ac.startActivityForResult(intentCamera, CREATE_PDF_REQUEST_CODE);
        } else {

            Intent intentGallery = new Intent(ac, EditScanWrapperActivity.class);
            Intent intentFileExplorer = new Intent(ac, SyncFileExplorer.class);

            Intent[] extraIntents = new Intent[2];
            extraIntents[0] = intentGallery;
            extraIntents[1] = intentFileExplorer;

            // Create file chooser intent
            Intent chooserIntent = Intent.createChooser(intentCamera, ac.getString(R.string.choose_pdf));
            chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
            ac.startActivityForResult(chooserIntent, CREATE_PDF_REQUEST_CODE);
        }
    }


    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CREATE_PDF_REQUEST_CODE  && resultCode == RESULT_OK) {

            // si salio apretando aceptar, pero con un documento vacio
            docEditId = data.getStringExtra("doc_edit_id");
            if(docEditId!=null && docEditId.equals("-1")) {
                docString = "";
                docByteArray = new byte[0];
                fileName = "";
                synchronized (rr) {
                    rr.notify();
                    rr = null;
                }
                return;
            }

            // si se cancelo mediante back button
            fileUri = data.getParcelableExtra("pdf_uri");
            if(fileUri==null){
                failureUnlock();
                return;
            }

            // si salio apretando el boton aceptar, con un documento generado
            fileName = getFileName(fileUri);
            new Thread(new Runnable() {
                @Override
                public void run() {

                    final byte[] docBytes;
                    try {
                        //docBytes = FileUtils.readFileToByteArray(new File(pdfUri.getEncodedPath()));
                        docBytes = IOUtils.toByteArray( ac.getContentResolver().openInputStream(fileUri));
                        String base64String = Base64.encodeToString(docBytes, Base64.NO_WRAP);
                        docString = base64String;
                        docByteArray = docBytes;

                        synchronized(rr)
                        {
                            rr.notify();
                            rr = null;
                        }

                    } catch (Exception e) {
                        //e.printStackTrace();
                        failureUnlock();
                    }
                }
            }).start();

        } else {
            // por si sale sin resultado, hay que desbloquear
            failureUnlock();
        }
    }


    private void failureUnlock(){
        try {
            synchronized (rr) {
                rr.notify();
            }
        } catch (Exception e) {
        } finally {
            rr = null;
            docString = null;
            docByteArray = null;
        }
    }


    public void release(){
        try {
            synchronized (rr) {
                rr.notify();
            }
        } catch (Exception e) {
        } finally {
            rr = null;
            docString = null;
            docByteArray = null;
        }
    }


    private String getFileName(Uri uri) {
        String displayName = null;
        try {
            String uriString = uri.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = ac.getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
            }
        } catch (Exception e) {
        }
        return displayName;
    }


}
