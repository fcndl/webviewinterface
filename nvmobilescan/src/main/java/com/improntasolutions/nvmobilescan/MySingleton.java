/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

/**
 * Created by flezcano on 17/10/2017.
 */

import android.content.Context;
import android.util.Log;

import java.util.HashMap;

/**
 * Created by flezcano on 17/10/2017.
 */

public class MySingleton {
    Context context;
    private static volatile MySingleton sInstance;

    public HashMap<String, Object> data = new HashMap<String, Object>();



    //private constructor.
    private MySingleton() {

        //Prevent form the reflection api.
        if (sInstance != null) {
            throw new RuntimeException("Use getInstance() method to get the single instance of this class.");
        }
    }

    public static MySingleton getInstance(Context ctx) {
        //Double check locking pattern
        if (sInstance == null) { //Check for the first time
            Log.d("MySingleton", "Init...");
            synchronized (MySingleton.class) {   //Check for the second time.
                //if there is no instance available... create new one
                if (sInstance == null) {
                    sInstance = new MySingleton();
                    sInstance.init(ctx);
                }
            }
        }

        return sInstance;
    }


    public void init(Context ctx) {
        context = ctx;
    }

}