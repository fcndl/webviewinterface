/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;


import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Rect;
import android.hardware.Camera;
import android.media.AudioManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.OrientationEventListener;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TableLayout;
import android.widget.TextView;



import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static android.hardware.Camera.Parameters.WHITE_BALANCE_AUTO;
import static android.view.Gravity.CENTER;


public class CameraCaptureActivity extends AppCompatActivity {
    static final String TAG = "CameraCaptureActivity";

    private Camera mCamera;
    private CameraPreview mPreview;
    ImageButton btnCapture;
    private CustomOrientationEventListener orientationListener;

    int rotation;
    boolean cameraBusy = false;
    //MySingleton singleton;
    ImageButton flashButton;
    ImageButton resButton;


    LinearLayout lo;

    boolean FLASH_MODE_OFF_SUPPORTED;
    boolean FLASH_MODE_ON_SUPPORTED;
    boolean FLASH_MODE_AUTO_SUPPORTED;
    boolean FLASH_MODE_TORCH_SUPPORTED;


    FrameLayout flCam;
    TextView anchorView;
    LinearLayout cameraLayout;
    ScrollView sv;
    SharedPreferences settings;



    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera_capture);

        Toolbar toolBar = (Toolbar)findViewById(R.id.activity_toolbar);
        setSupportActionBar(toolBar);


        settings = getApplicationContext().getSharedPreferences("scan_preferences_file", getApplicationContext().MODE_PRIVATE);
        orientationListener = new CustomOrientationEventListener(getBaseContext());
        //singleton = MySingleton.getInstance(this);
        resButton =  (ImageButton) findViewById(R.id.resolutionButton);
        flashButton = (ImageButton) findViewById(R.id.flashButton);
        flashButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mCamera==null){
                    return;
                }

                showFlashLightPopUpMenu();

            }
        });


        flCam = (FrameLayout)findViewById(R.id.camera_preview);
        anchorView  = (TextView) findViewById(R.id.anchorView);
        cameraLayout = (LinearLayout) findViewById(R.id.camera_capture_root_layout);
        resButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                showPopUp();
            }
        });



        //final Uri fileUri = getIntent().getParcelableExtra("FILE_URI");
        //if(fileUri==null){
        //    Log.e(TAG, "No se ha especificado Uri para la captura");
        //    return;
        //}

        final Camera.PictureCallback mPicture = new Camera.PictureCallback() {

            @Override
            public void onPictureTaken(final byte[] data, Camera camera) {

                try {

                    File folder = Utils.getCacheFolder(getApplicationContext());
                    File outputFile = null;
                    try {
                        outputFile = File.createTempFile("tmp", ".jpg", folder);
                    } catch (IOException e) {
                        Log.e("CameraCaptureActivity", e.getMessage());
                    }
                    FileOutputStream out = null;
                    try {
                        out = new FileOutputStream(outputFile);
                        out.write(data);
                    } catch (Exception e) {
                        e.printStackTrace();
                    } finally {
                        try {
                            if (out != null) {
                                out.close();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                    Uri furi = Uri.fromFile(outputFile);


                    //buscar el id de la backfacing camera
                    for (int i = 0; i < Camera.getNumberOfCameras(); i++) {
                        Camera.CameraInfo cInfo = new Camera.CameraInfo();
                        Camera.getCameraInfo(i, cInfo);
                        if (cInfo.facing == Camera.CameraInfo.CAMERA_FACING_BACK) {
                            rotation = (cInfo.orientation + orientationListener.prevOrientation) % 360;
                        }
                    }

                    // obtener la rotacion necesaria para mostrar la foto
                    // en la forma que el usuario la ve
                    Intent intent = new Intent();
                    intent.putExtra("rotation", rotation);
                    intent.putExtra("file_uri", furi);
                    setResult(RESULT_OK, intent);
                    finish();


                } catch (Exception e) {
                    Log.d(TAG, "Error accessing file: " + e.getMessage());
                    cameraBusy = false;
                }
            }

        };

        mPreview = new CameraPreview(this, null);
        FrameLayout cameraPreview = (FrameLayout) findViewById(R.id.camera_preview);
        cameraPreview.addView(mPreview);

        btnCapture = (ImageButton) findViewById(R.id.button_capture);

        // Add a listener to the Capture button
        btnCapture.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // get an image from the camera
                        if(cameraBusy || mCamera==null){
                            return;
                        }
                        cameraBusy = true;
                        mCamera.takePicture(shutterCallback, null, mPicture);
                    }
                }
        );
    }

    private final Camera.ShutterCallback shutterCallback = new Camera.ShutterCallback() {
        public void onShutter() {
            AudioManager mgr = (AudioManager) getSystemService(Context.AUDIO_SERVICE);
            mgr.playSoundEffect(AudioManager.FLAG_PLAY_SOUND);
        }
    };



    public void setFlashLightMode(View v){

        if(mCamera==null){
            return;
        }

        Camera.Parameters params = mCamera.getParameters();
        SharedPreferences.Editor editor = settings.edit();

        // desactivado
        if (v.getId()==R.id.rb1){
            flashButton.setImageResource(R.drawable.ic_flash_off);
            if(FLASH_MODE_OFF_SUPPORTED) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                editor.putInt("selected_flash_mode",0);
                editor.commit();
            }
        }

        //activado
        if (v.getId()==R.id.rb2){
            flashButton.setImageResource(R.drawable.ic_flash_on);
            if(FLASH_MODE_ON_SUPPORTED) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                editor.putInt("selected_flash_mode",1);
                editor.commit();
            }
        }

        //automatico
        if (v.getId()==R.id.rb3){
            flashButton.setImageResource(R.drawable.ic_flash_auto);
            if(FLASH_MODE_AUTO_SUPPORTED) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                editor.putInt("selected_flash_mode",2);
                editor.commit();
            }
        }

        // torch
        if (v.getId()==R.id.rb4){
            flashButton.setImageResource(R.drawable.ic_flash_torch);
            if (FLASH_MODE_TORCH_SUPPORTED) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                editor.putInt("selected_flash_mode",3);
                editor.commit();
            }
        }

        mCamera.setParameters(params);

        if(flashLightPopUp!=null && flashLightPopUp.isVisible()) {
            flashLightPopUp.dismissAllowingStateLoss();
        }
    }




    FlashLightPopUpDialog flashLightPopUp;
    ResolutionsPopUpDialog resolutionsPopUp = new ResolutionsPopUpDialog();

    public void showFlashLightPopUpMenu(){

        int rotation = orientationListener.prevOrientation; //en realidad es la orientacion actual
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        flashLightPopUp = new FlashLightPopUpDialog();
        flashLightPopUp.rotation = rotation;
        flashLightPopUp.setArguments(args);
        //flashLightPopUp.show(ft, "message");
        ft.add(flashLightPopUp,"message")
                .commitAllowingStateLoss();

    }



    public void showPopUp(){

        int rotation = orientationListener.prevOrientation; //en realidad es la orientacion actual
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Bundle args = new Bundle();
        resolutionsPopUp.rotation = rotation;

        resolutionsPopUp.width = (int)(0.5*flCam.getWidth());
        resolutionsPopUp.height = (int)(0.6*flCam.getHeight());

        //resolutionsPopUp.setArguments(args);
        ft.add(resolutionsPopUp,"message")
                .commitAllowingStateLoss();

    }




    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open(); // attempt to get a Camera instance
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
        }
        return c; // returns null if camera is unavailable
    }

    public class CustomOrientationEventListener extends OrientationEventListener {

        private static final String TAG = "CustomOrientationEvent";
        private Context context;
        private int prevOrientation = 0;
        private int currentRotation=0;


        public CustomOrientationEventListener(Context context) {
            super(context);
            this.context = context;
        }

        @Override
        public void onOrientationChanged(int orientation) {

            if (orientation == OrientationEventListener.ORIENTATION_UNKNOWN ){
                return;
            }
            // por mas que el sistema tenga configurada la no rotacion, nosotros la usaremos iguaal en la app de camara
            //int newOrientation = (orientation + 45) / 90 * 90;
            int newOrientation = -1;
            if ((orientation < 35 || orientation > 325)) { // PORTRAIT
                newOrientation = 0;
            } else if (orientation > 145 && orientation < 215) { // REVERSE PORTRAIT
                newOrientation = 180;
            } else if (orientation > 55 && orientation < 125 ) { //REVERSE LANDSCAPE
                newOrientation = 90;
            } else if (orientation > 235 && orientation < 305) { //LANDSCAPE
                newOrientation = 270;
            }

            if (newOrientation==-1){
                return;
            }

            if (prevOrientation != newOrientation ) {
                onSimpleOrientationChanged(newOrientation);
                prevOrientation = newOrientation;
            }
        }


        public  void onSimpleOrientationChanged( int orientation) {

            int previous = prevOrientation;

            if(previous==270 && orientation ==0  ){
                prevOrientation = 0;
                currentRotation = currentRotation-90;

            } else if (previous==0 && orientation ==270){
                prevOrientation = 270;
                currentRotation = currentRotation+90;

            } else {
                prevOrientation = orientation;
                currentRotation = currentRotation-orientation+previous;
                if(prevOrientation <0 || prevOrientation>=360){
                    int a =1;
                }
            }

            animateButtons(currentRotation);
            updateMenuOrientation();
        }

    }


    public void animateButtons(int rot) {
        btnCapture.animate().rotation(rot).setDuration(500).start();
        flashButton.animate().rotation(rot).setDuration(500).start();
        resButton.animate().rotation(rot).setDuration(500).start();
    }



    public void updateMenuOrientation(){

        if (resolutionsPopUp != null && resolutionsPopUp.isVisible()) {
            resolutionsPopUp.dismissAllowingStateLoss();
            showPopUp();
        }
        if (flashLightPopUp != null && flashLightPopUp.isVisible()) {
            flashLightPopUp.dismissAllowingStateLoss();
            showFlashLightPopUpMenu();
        }
    }




    @Override
    protected void onResume() {
        super.onResume();
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        orientationListener.enable();
        startCamera();
    }

    @Override
    protected void onPause() {
        super.onPause();
        orientationListener.disable();
        releaseCamera();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void releaseCamera(){
        if (mCamera != null) {
            mCamera.stopPreview();
            mPreview.setCamera(null);
            mCamera.release();
            mCamera = null;
        }



    }

    public void startCamera(){
        mCamera = getCameraInstance();
        mCamera.setDisplayOrientation(90);

        // get Camera parameters
        Camera.Parameters params = mCamera.getParameters();

        // Set best quality
        if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
        } else if (params.getSupportedFocusModes().contains(Camera.Parameters.FOCUS_MODE_AUTO)) {
            params.setFocusMode(Camera.Parameters.FOCUS_MODE_AUTO);
        }

        // CON ESTO, NO ME ANDA EL FLASH en la app
        /*if (params.getSupportedSceneModes().contains(Camera.Parameters.SCENE_MODE_HDR)) {
            params.setSceneMode(Camera.Parameters.SCENE_MODE_HDR);
        }*/

        params.setWhiteBalance(WHITE_BALANCE_AUTO);
        params.setJpegQuality(100);

        if(!hasFlash()){
            flashButton.setVisibility(View.GONE);
        } else {

            if (params.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_AUTO)){
                FLASH_MODE_AUTO_SUPPORTED = true;
            }
            if (params.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_OFF)){
                FLASH_MODE_OFF_SUPPORTED = true;
            }
            if (params.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_ON)){
                FLASH_MODE_ON_SUPPORTED = true;
            }
            if (params.getSupportedFlashModes().contains(Camera.Parameters.FLASH_MODE_TORCH)){
                FLASH_MODE_TORCH_SUPPORTED = true;
            }


            int selectFlashMode = settings.getInt("selected_flash_mode",2);

            if( selectFlashMode==1 && FLASH_MODE_ON_SUPPORTED) {
                params.setFlashMode(Camera.Parameters.FLASH_MODE_ON);
                flashButton.setImageResource(R.drawable.ic_flash_on);
            } else if( selectFlashMode==2 && FLASH_MODE_AUTO_SUPPORTED){
                params.setFlashMode(Camera.Parameters.FLASH_MODE_AUTO);
                flashButton.setImageResource(R.drawable.ic_flash_auto);
            }else if( selectFlashMode==3 && FLASH_MODE_TORCH_SUPPORTED){
                params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
                flashButton.setImageResource(R.drawable.ic_flash_torch);
            } else { //0
                params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
                flashButton.setImageResource(R.drawable.ic_flash_off);
            }
        }



        //5mp 2560 width, 1920 h
       //5MP by default
        Camera.Size size= mCamera.new Size(0, 0);

        int selectedResolution = settings.getInt("selected_resolution", -1);

        if  (selectedResolution==-1) {
            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            SharedPreferences.Editor editor = settings.edit();


            for (int i = 0; i < sizes.size(); i++) {
                if (sizes.get(i).width >= size.width && sizes.get(i).width <= 2560)
                    if (sizes.get(i).height >= size.height && sizes.get(i).height <= 1980) {
                        size = sizes.get(i);
                        editor.putInt("selected_resolution", i);
                        editor.commit();
                        selectedResolution = i;
                    }
            }

            if(size==null){
                size = sizes.get(0);
                selectedResolution = 0;
                editor.putInt("selected_resolution", 0);
                editor.commit();
            }



        } else {
            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            size = sizes.get(selectedResolution);
        }



        if(lo==null) {

            lo = new LinearLayout(this);
            lo.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.MATCH_PARENT));
            lo.setOrientation(LinearLayout.VERTICAL);

            sv = new ScrollView(this);
            sv.setLayoutParams(new ScrollView.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.MATCH_PARENT));

            final RadioGroup rgroup = new RadioGroup(this);
            rgroup.setLayoutParams(new TableLayout.LayoutParams(TableLayout.LayoutParams.MATCH_PARENT, TableLayout.LayoutParams.MATCH_PARENT));

            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            for (int i = 0; i < sizes.size(); i++) {

                RadioButton rbutton = new RadioButton(this);
                rbutton.setTag(i);
                rbutton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        setCameraResolution(Integer.parseInt(view.getTag().toString()));

                        // deschequear el resto
                        //rgroup.clearCheck();
                        if(resolutionsPopUp!=null && resolutionsPopUp.isVisible()) {
                            resolutionsPopUp.dismissAllowingStateLoss();
                        }
                    }
                });

                rbutton.setText(String.valueOf(sizes.get(i).width) + "x" + String.valueOf(sizes.get(i).height));
                rbutton.setGravity(CENTER);

                rgroup.addView(rbutton);
                if(i==selectedResolution){
                    rbutton.setChecked(true);
                    /*if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
                        rbutton.setId(CameraUtils.generateViewId());
                    } else {
                        rbutton.setId(View.generateViewId());
                    }
                    rgroup.check(rbutton.getId());*/
                }
            }

            lo.addView(rgroup);
            sv.addView(lo);
            resolutionsPopUp.content = sv;
        }


        // max resolution
        /*List<Camera.Size > sizes = params.getSupportedPictureSizes();
        Camera.Size size = sizes.get(0);
        for (int i = 1; i < sizes.size(); i++) {
            if (sizes.get(i).width > size.width )
                if (sizes.get(i).height > size.height)
                    size = sizes.get(i);
        }*/

        params.setPictureSize(size.width, size.height);
        mCamera.setParameters(params);


        mPreview.setCamera(mCamera);
        mPreview.startPreview();

    }




    public  void setCameraResolution(int order){
        //try {
            Camera.Parameters params = mCamera.getParameters();
            List<Camera.Size> sizes = params.getSupportedPictureSizes();
            Camera.Size size;
            size = sizes.get(order);

            if (size != null) {
                params.setPictureSize(size.width, size.height);
                mCamera.setParameters(params);

                SharedPreferences.Editor editor = settings.edit();
                editor.putInt("selected_resolution", order);
                editor.commit();
            }
        //}catch (Exception e){
        //    Log.e("CameraCaptureActivity", e.getMessage());
        //}

    }



    public boolean hasFlash() {
        if (mCamera == null) {
            return false;
        }
        return this.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);

        /*Camera.Parameters parameters = mCamera.getParameters();
        if (parameters.getFlashMode() == null) {
            return false;
        }
        List<String> supportedFlashModes = parameters.getSupportedFlashModes();
        if (supportedFlashModes == null || supportedFlashModes.isEmpty() || supportedFlashModes.size() == 1 && supportedFlashModes.get(0).equals(Camera.Parameters.FLASH_MODE_OFF)) {
            return false;
        }
        return true;*/
    }


    public static Rect locateView(View v)
    {
        int[] loc_int = new int[2];
        if (v == null) return null;
        try
        {
            v.getLocationOnScreen(loc_int);
        } catch (NullPointerException npe)
        {
            //Happens when the view doesn't exist on screen anymore.
            return null;
        }
        Rect location = new Rect();
        location.left = loc_int[0];
        location.top = loc_int[1];
        location.right = location.left + v.getWidth();
        location.bottom = location.top + v.getHeight();
        return location;
    }

    @Override
    public void onStop(){
        super.onStop();
    }


}