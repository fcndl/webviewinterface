/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Parcelable;
import android.provider.OpenableColumns;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.util.Base64;
import android.webkit.JavascriptInterface;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javax.security.auth.callback.Callback;

import static android.app.Activity.RESULT_OK;
import static android.content.Context.MODE_PRIVATE;

/**
 * Created by flezcano on 03/04/2018.
 */



public class NvMobileScan {

    Activity ac;

    public int CAPTURE_CODE = 1555;
    public int EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE = 1560;

    public int CAMERA_PERMISSION_REQUEST_CODE = 1561;

    // parametros scan pdf
    Integer ppi;
    String fileDesc;
    Integer default_ppi;
    String default_path;
    String path;

    // archivo salida
    Uri fileUri;


    public interface ScanResultCallback<T>{
        void run(T data);
    }

    ScanResultCallback callback;

    public NvMobileScan(Activity ac) {
        this.ac = ac;

        default_ppi = 120;
        String path_internal_storage = ac.getFilesDir().getAbsolutePath();
        default_path = path_internal_storage;
    }

    public void setDefault_ppi(Integer defaultDpi) {
        this.default_ppi = defaultDpi;
    }

    public Integer getDefault_ppi() {
        return default_ppi;
    }

    public void setDefault_path(String default_path) {
        this.default_path = default_path;
    }

    public String getDefault_path() {
        return default_path;
    }


    public void OpenGallery(Integer inputPpi, String path, String inputFileDesc, ScanResultCallback callback) {

        String depthcolor = "truecolor";
        this.ppi = inputPpi;
        this.fileDesc = inputFileDesc;
        this.path = path;
        this.callback = callback;
        if (PermissionsUtils.checkPermissionForExternalStorage(ac)) {

            int REQUEST_CODE = CAPTURE_CODE;
            int preference = ScanConstants.OPEN_MEDIA;
            Intent intent = new Intent(ac, ScanActivity.class);
            intent.putExtra("fileDesc", fileDesc);
            intent.putExtra("ppi", ppi);
            intent.putExtra("default_ppi", default_ppi);
            intent.putExtra("path", path);
            intent.putExtra("default_path", default_path);
            intent.putExtra("depthcolor", depthcolor);
            intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            ac.startActivityForResult(intent, REQUEST_CODE);
        } else {
            PermissionsUtils.requestPermissionForExternalStorage(ac, EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE);
        }
    }

    public void OpenGallery(ScanResultCallback callback) {
        OpenGallery(null, null, null, callback);
    }


    public void OpenScanner(Integer inputPpi, String path, String inputFileDesc, ScanResultCallback callback) {
        String depthcolor = "truecolor";
        this.ppi = inputPpi;
        this.fileDesc = inputFileDesc;
        this.path = path;
        this.callback = callback;
        if (PermissionsUtils.checkPermissionForCamera(ac)) {
            int REQUEST_CODE = CAPTURE_CODE;
            int preference = ScanConstants.OPEN_CAMERA;
            Intent intent = new Intent(ac, ScanActivity.class);
            intent.putExtra("fileDesc", fileDesc);
            intent.putExtra("ppi", ppi);
            intent.putExtra("default_ppi", default_ppi);
            intent.putExtra("path", path);
            intent.putExtra("default_path", default_path);
            intent.putExtra("depthcolor", depthcolor);
            intent.putExtra(ScanConstants.OPEN_INTENT_PREFERENCE, preference);
            ac.startActivityForResult(intent, REQUEST_CODE);
        } else {
            PermissionsUtils.requestPermissionForCamera(ac, CAMERA_PERMISSION_REQUEST_CODE);
        }
    }

    public void OpenScanner(ScanResultCallback callback){
        OpenScanner(null, null, null, callback);
    }



    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        //super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAPTURE_CODE) {
            if (resultCode == RESULT_OK) {

                fileUri = data.getParcelableExtra("pdf_uri");
                if (fileUri != null) {

                    final String fileName = getFileName(fileUri);
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                //docBytes = FileUtils.readFileToByteArray(new File(fileUri.getEncodedPath()));
                                final byte[] docBytes = IOUtils.toByteArray(ac.getContentResolver().openInputStream(fileUri));
                                final String base64String = Base64.encodeToString(docBytes, Base64.NO_WRAP);

                                ac.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        if (callback != null) {
                                            Map data = new HashMap<String, Object>();
                                            data.put("fileName", fileName);
                                            data.put("fileContent", base64String);
                                            callback.run(data);
                                        }
                                        return;
                                    }
                                });

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }).start();
                    return;
                }
            }

            // en caso de error se devuelve map vacio
            if (callback != null) {
                Map res = new HashMap<String, Object>();
                res.put("fileName", "");
                res.put("fileContent", "");
                callback.run(data);
            }
        }
    }




    private String getFileName(Uri uri) {
        String displayName = null;
        try {
            String uriString = uri.toString();
            File myFile = new File(uriString);
            String path = myFile.getAbsolutePath();
            if (uriString.startsWith("content://")) {
                Cursor cursor = null;
                try {
                    cursor = ac.getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName = myFile.getName();
            }
        } catch (Exception e) {
        }
        return displayName;
    }


    /**
     * Método que implementa el evento onRequestPermissionsResult para el resultado de la solicitud
     * permisos de cámara y acceso a almacenamiento externo que necesita la clase NvMobileScan. Debe
     * ser llamado obligatoriamente desde el callback onRequestPermissionsResult de la actividad host
     * del objeto de clase NvMobileScan
     *
     * @param requestCode  variable requestCode que arroja el callback onRequestPermissionsResult
     *                     de la actividad host
     *                     del objeto de clase NvMobileScan
     * @param permissions  variable permissions que arroja el callback onRequestPermissionsResult
     *                     de la actividad host
     *                     el objeto de clase NvMobileScan
     * @param grantResults variable grantResults que arroja el callback onRequestPermissionsResult
     *                     de la actividad host
     *                     el objeto de clase NvMobileScan
     */
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == CAMERA_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {
                    OpenScanner(this.ppi, this.path, this.fileDesc,this.callback);
                    return;

                } else {

                    if (ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.CAMERA) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay

                    } else {

                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(ac, false);
                    }
                }
            }
        }

        if (requestCode == EXTERNAL_STORAGE_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    OpenGallery(this.ppi, this.path, this.fileDesc,this.callback);
                    return;

                } else {


                    if (ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.READ_EXTERNAL_STORAGE) ||
                            ActivityCompat.shouldShowRequestPermissionRationale(ac, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {

                        // Mostrar algun mensaje que explique que requiere de los permisos: no usar Toast para evitar bug de screen overlay

                    } else {
                        // el usuario selecciono never ask again... preguntarle si desea ir a la config de permisos
                        PermissionsUtils.showAppConfigAlert(ac, false);
                    }

                }
            }
        }
    }


}
