/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 *
 * This software is based on the library ScanLibrary available at
 * https://github.com/jhansireddy/AndroidScannerDemo
 *
 * Copyright (c) 2016 Jhansi Karee
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 */

package com.improntasolutions.nvmobilescan;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.ClipDescription;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v7.view.ActionMode;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.RelativeSizeSpan;
import android.util.Log;
import android.view.DragEvent;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;




import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import it.sephiroth.android.library.tooltip.Tooltip;

import static com.improntasolutions.nvmobilescan.ScanActivity.PREFS_NAME;


public class PickImageFragment extends Fragment {


    private View view;
    private ImageButton cameraButton;
    private ImageButton galleryButton;
    private ImageButton finishButton;
    private ImageButton deleteButton;
    LinearLayout optionsHintLayout;
    LinearLayout buttonsHintLayout;


    private Uri fileUri;
    private IScanner scanner;
    LinearLayout buttonPannel;
    LinearLayout buttonPannel2;
    static final int MY_PERMISSIONS_READ_WRITE_EXTERNAL_STORAGE = 10;
    static final int MY_PERMISSIONS_CAMERA = 11;


    private boolean wasDestroyed = false;


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        if (!(activity instanceof IScanner)) {
            throw new ClassCastException("Activity must implement IScanner");
        }
        this.scanner = (IScanner) activity;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.pick_image_fragment, null);

        if (savedInstanceState != null) {
            if (savedInstanceState.containsKey("wasDestroyed")) {
                wasDestroyed = savedInstanceState.getBoolean("wasDestroyed", false);
                fileUri = savedInstanceState.getParcelable("fileUri");
            }
        }
        String fileDesc = ((ScanActivity) getActivity()).fileDesc;

        Toolbar toolBarTop = (Toolbar) view.findViewById(R.id.activity_toolbar);
        ((AppCompatActivity) getActivity()).setSupportActionBar(toolBarTop);
        ((AppCompatActivity) getActivity()).getSupportActionBar().setDisplayShowTitleEnabled(false);
        toolBarTop.setTitle(fileDesc);

        setHasOptionsMenu(true);

        init();
        return view;
    }


    private void init() {
        cameraButton = (ImageButton) view.findViewById(R.id.cameraButton);
        cameraButton.setOnClickListener(new CameraButtonClickListener());
        galleryButton = (ImageButton) view.findViewById(R.id.selectButton);
        galleryButton.setOnClickListener(new GalleryClickListener());
        finishButton = (ImageButton) view.findViewById(R.id.finishButton);
        finishButton.setOnClickListener(new FinishButtonClickListener());
        deleteButton = (ImageButton) view.findViewById(R.id.deleteButton);
        deleteButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (gridview != null && gridview.getCount() > 0) {
                    if (adapter.getSelectedIndexes().size() > 0) {
                        deleteSelectedItems();
                    } else {
                        Toast.makeText(getActivity(), "No se seleccionaron elementos", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });

        buttonPannel = (LinearLayout) view.findViewById(R.id.buttonPannel);
        buttonPannel2 = (LinearLayout) view.findViewById(R.id.buttonPannel2);
        optionsHintLayout = view.findViewById(R.id.options_hints_layout);
        buttonsHintLayout = view.findViewById(R.id.buttons_hints_layout);

        // mostrar imagenes cargadas, si es que hay
        loadImages();

        // si es la primera vez que ingresa
        if (!wasDestroyed) {

            // si se ingresa con preferencia...
            if (isIntentPreferenceSet()) {
                if (!((ScanActivity) getActivity()).intentPreferenceAlreadyHandled) {
                    ((ScanActivity) getActivity()).intentPreferenceAlreadyHandled = true;
                    handleIntentPreference();
                }
            }
        }
    }


    public void handleIntentPreference() {
        int preference = getIntentPreference();
        if (preference == ScanConstants.OPEN_CAMERA) {
            openCamera();
        } else if (preference == ScanConstants.OPEN_MEDIA) {
            openMediaContent();
        }
    }


    private boolean isIntentPreferenceSet() {
        int preference = getArguments().getInt(ScanConstants.OPEN_INTENT_PREFERENCE, 0);
        return preference != 0;
    }


    private int getIntentPreference() {
        int preference = getArguments().getInt(ScanConstants.OPEN_INTENT_PREFERENCE, 0);
        return preference;
    }


    private class CameraButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                openCamera();
                return;
            }

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

                requestPermissions(new String[]{
                                Manifest.permission.CAMERA,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        MY_PERMISSIONS_CAMERA);
            } else {
                openCamera();
            }
        }
    }


    private class GalleryClickListener implements View.OnClickListener {
        @Override
        public void onClick(View view) {

            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                openMediaContent();
                return;
            }

            if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED ||
                    ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                //ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_READ_WRITE_EXTERNAL_STORAGE);
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE}, MY_PERMISSIONS_READ_WRITE_EXTERNAL_STORAGE);
            } else {
                openMediaContent();
            }
        }
    }


    private class FinishButtonClickListener implements View.OnClickListener {
        @Override
        public void onClick(View v) {

            final ArrayList<String> uriList = ((ScanActivity) getActivity()).uriList;
            if (uriList.size() == 0) {

                Intent data = new Intent();
                data.putExtra("pdf_uri", Uri.EMPTY);
                data.putExtra("doc_edit_id", "-1");
                getActivity().setResult(Activity.RESULT_OK, data);
                getActivity().finish();
                return;
            }
            //String fileDesc = getActivity().getIntent().getExtras().getString("fileDesc", "");
            String depthcolor = getActivity().getIntent().getExtras().getString("depthcolor", "truecolor");

            Integer resultPpi = 120;
            Integer ppi = getActivity().getIntent().getExtras().getInt("ppi", 0);
            Integer default_ppi = getActivity().getIntent().getExtras().getInt("default_ppi", 0);
            if (ppi > 0) {
                resultPpi = ppi;
            } else if (default_ppi > 0) {
                resultPpi = default_ppi;
            }

            // El pdf obtenido, se guarda en el path especificado
            String resultPath = "";
            String path = getActivity().getIntent().getExtras().getString("path", "");
            String default_path = getActivity().getIntent().getExtras().getString("default_path", "");

            // si no se especifica path, ni path por defecto, se guardará en la cache externa
            if (!path.equals("")) {
                resultPath = path;
            } else {
                String timeStamp = new SimpleDateFormat("ddMMyyyy_HHmmss.sss").format(new
                        Date());
                String filename = "doc_" + timeStamp + ".pdf";

                if (!default_path.equals("")) {
                    if (!default_path.endsWith("/")) {
                        default_path += "/";
                    }
                    resultPath = default_path + filename;
                } else {

                    File folder = Utils.getCacheFolder(getActivity());
                    resultPath = new File(folder, filename).getPath();
                    //resultPath = getActivity().getExternalCacheDir().getAbsolutePath() + "/" + filename;
                }
            }

            final ArrayList<PagePreference> pagesPreferences = ((ScanActivity) getActivity()).pagesPreferences;
            final ArrayList<String> originalUriList = ((ScanActivity) getActivity()).originalUriList;
            final String fileDesc = ((ScanActivity) getActivity()).fileDesc;
            new createPDFTask((ScanActivity) getActivity(), resultPath, uriList, originalUriList, pagesPreferences, fileDesc, resultPpi, depthcolor).execute();
        }
    }


    public void openMediaContent() {

        // galeria
        Intent intent = new Intent(Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, ScanConstants.PICKFILE_REQUEST_CODE);
    }


    public final static int START_MY_CAMERA_REQUEST_CODE = 788;


    public void openCamera() {
        Intent cameraIntent = new Intent(this.getActivity(), CameraCaptureActivity.class);
        startActivityForResult(cameraIntent, START_MY_CAMERA_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.d("", "onActivityResult" + resultCode);
        Bitmap bitmap = null;
        if (resultCode == Activity.RESULT_OK) {

            try {
                switch (requestCode) {
                    //case ScanConstants.START_CAMERA_REQUEST_CODE:
                    //    bitmap = getBitmap(fileUri);
                    //    break;

                    case ScanConstants.PICKFILE_REQUEST_CODE:
                        //bitmap = getBitmapFromGallery(data.getData());
                        //bitmap = getBitmap(data.getData());
                        fileUri = data.getData();
                        postImagePick2(fileUri, 0);
                        break;

                    case START_MY_CAMERA_REQUEST_CODE:
                        int rotation = data.getIntExtra("rotation", 0);
                        Uri furi = data.getParcelableExtra("file_uri");
                        postImagePick2(furi, rotation);
                        break;

                }
            } catch (Exception e
                    ) {
                e.printStackTrace();
            }
        } else {
            if (requestCode == ScanConstants.PICKFILE_REQUEST_CODE || requestCode == START_MY_CAMERA_REQUEST_CODE) {
                // si se volvio sin resultado de estos eventos
                //  y si no hay elementos en la lista, salir del modulo de scan
                ArrayList<String> uriList = ((ScanActivity) getActivity()).uriList;
                if (uriList.size() == 0 && isIntentPreferenceSet()) {
                    getActivity().finish();
                }
            }
        }
    }


    protected void postImagePick(Bitmap bitmap) {
        Uri uri = Utils.getUri(getActivity(), bitmap);
        bitmap.recycle();
        scanner.onBitmapSelect(uri);
    }


    protected void postImagePick2(Uri uri, int rotation) {
        scanner.onBitmapSelect2(uri, rotation);
    }


    private Bitmap getBitmap(Uri selectedimg) throws IOException {
        ExifInterface ei = null;
        try {
            ei = new ExifInterface(selectedimg.getPath());
        } catch (IOException e) {
            e.printStackTrace();
        }

        int orientation = ExifInterface.ORIENTATION_UNDEFINED;
        if (ei != null) {
            orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
        }

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inSampleSize = 3;
        AssetFileDescriptor fileDescriptor = null;
        fileDescriptor =
                getActivity().getContentResolver().openAssetFileDescriptor(selectedimg, "r");
        Bitmap original
                = BitmapFactory.decodeFileDescriptor(
                fileDescriptor.getFileDescriptor(), null, options);


        Bitmap bitmapFixed = null;
        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                bitmapFixed = rotateImage(original, 90.0f);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                bitmapFixed = rotateImage(original, 180.0f);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                bitmapFixed = rotateImage(original, 270.0f);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                bitmapFixed = original;
                break;
        }
        if (original != bitmapFixed) {
            original.recycle();
        }

        return bitmapFixed;
    }


    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix,
                true);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_READ_WRITE_EXTERNAL_STORAGE: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED) {

                    openMediaContent();

                } else {
                }
                break;
            }
            case MY_PERMISSIONS_CAMERA: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 &&
                        grantResults[0] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[1] == PackageManager.PERMISSION_GRANTED &&
                        grantResults[2] == PackageManager.PERMISSION_GRANTED) {

                    openCamera();

                } else {

                }
                break;
            }
        }
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean("wasDestroyed", true);
        if (fileUri != null) {
            outState.putParcelable("fileUri", fileUri);
        }
    }


    CustomAdapter adapter;
    GridView gridview;
    ArrayList<ImageItem> imageItems;


    public void loadImages() {
        imageItems = new ArrayList<ImageItem>();
        ArrayList<String> uriList = ((ScanActivity) getActivity()).uriList;
        if (uriList.size() > 0) {

            // ocultar hints si estan visibles
            hideHints();

            for (int i = 0; i < uriList.size(); i++) {
                try {
                    ImageItem item = new ImageItem("" + i, uriList.get(i));
                    imageItems.add(item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            gridview = (GridView) view.findViewById(R.id.gridview);
            adapter = new CustomAdapter(getActivity(), imageItems);
            gridview.setAdapter(adapter);
            gridview.setOnItemLongClickListener(defaultGridListener);

            //defaultGridItemClickListener
            gridview.setOnItemClickListener(defaultGridItemClickListener);
            gridview.setOnDragListener(gridviewOnDragListener);
        } else {
            // mostrar hints si corresponde
            showHints();
        }
    }


    public void deleteSelectedItems() {

        List<Integer> indexes = adapter.getSelectedIndexes();

        if (indexes.size() == 0) {
            return;
        }

        int l = 0;
        ArrayList<String> uriList = ((ScanActivity) getActivity()).uriList;
        ArrayList<String> originalUriList = ((ScanActivity) getActivity()).originalUriList;


        for (int i = 0; i < indexes.size(); i++) {

            int key = indexes.get(i) - l;
            l++;

            imageItems.remove(key);
            uriList.remove(key);
            originalUriList.remove(key);
        }

        for (int i = 0; i < imageItems.size(); i++) {
            imageItems.get(i).setItem_header(String.valueOf(i));
        }

        adapter.notifyDataSetChanged();

        // salir de modo action
        mActionMode.finish();
    }


    ActionMode mActionMode;
    boolean checkAllMode = true;
    Button btnSelectAll;
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        // Called when the action mode is created; startActionMode() was called
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            // Inflate a menu resource providing context menu items
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.cab_menu, menu);

            View amView = getActivity().getLayoutInflater().inflate(R.layout.action_mode_custom_layout, null, false);
            TextView tv = amView.findViewById(R.id.tv_items_selected);
            tv.setText("0 elementos seleccionados");
            mode.setCustomView(amView);


            btnSelectAll = amView.findViewById(R.id.btnSelectAll);
            btnSelectAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    checkAllItems();
                }
            });

            // ocultar pannel
            buttonPannel.setVisibility(View.GONE);

            // ver panel 2
            buttonPannel2.setVisibility(View.VISIBLE);

            return true;
        }

        // Called each time the action mode is shown. Always called after onCreateActionMode, but
        // may be called multiple times if the mode is invalidated.
        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {

            return false; // Return false if nothing is done
        }

        // Called when the user selects a contextual menu item
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            if (item.getItemId() == R.id.delete_opt) {
                deleteSelectedItems();
                mode.finish(); // Action picked, so close the CAB
                return true;
            }
            return false;
        }

        // Called when the user exits the action mode
        @Override
        public void onDestroyActionMode(ActionMode mode) {

            mActionMode = null;

            gridview.setOnItemLongClickListener(defaultGridListener);


            // en el adapterview items deselecionar todos
            adapter.clearSelecteds();
            adapter.setCheckMode(false);


            int visibleChildCount = (gridview.getLastVisiblePosition() - gridview.getFirstVisiblePosition()) + 1;
            for (int i = 0; i < visibleChildCount; i++) {
                ((CheckableLayout) gridview.getChildAt(i)).setChecked(false);
                ((CheckableLayout) gridview.getChildAt(i)).hideCheck();
                ((CheckableLayout) gridview.getChildAt(i)).setOnDragListener(null);
                // escondecheck
            }

            buttonPannel2.setVisibility(View.GONE);
            buttonPannel.setVisibility(View.VISIBLE);
            gridview.setOnItemClickListener(defaultGridItemClickListener);
        }
    };


    private AdapterView.OnItemLongClickListener defaultGridListener = new AdapterView.OnItemLongClickListener() {
        @Override
        public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

            boolean res = setSelectionMode();

            if (res) {
                // click para seleccionar el elemento
                gridview.performItemClick(view, i, l);
                return true;
            } else {
                return false;
            }
        }
    };


    public boolean setSelectionMode() {

        if (mActionMode != null) {
            return false;
        }

        mActionMode = ((AppCompatActivity) (getActivity())).startSupportActionMode(mActionModeCallback);
        adapter.setCheckMode(true);

        // Setear callbacks para arrastre
        // setear la escucha de arraste para cada elemento de la grilla
        //myDragEventListener mDragListen = new myDragEventListener();
        int visibleChildCount = (gridview.getLastVisiblePosition() - gridview.getFirstVisiblePosition()) + 1;
        for (int i = 0; i < visibleChildCount; i++) {
            final View vv = gridview.getChildAt(i);
            ((CheckableLayout) gridview.getChildAt(i)).showCheck();
        }

        // Seteando de esta manera no funciona
        // setear receptor de arrastre para la grilla
        gridview.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int pos, long id) {

                ClipData.Item item = new ClipData.Item(String.valueOf(pos) + "," + String.valueOf(id));
                final ClipData dragData = new ClipData("dragLabel", new String[]{ClipDescription.MIMETYPE_TEXT_PLAIN, ClipDescription.MIMETYPE_TEXT_PLAIN}, item);
                view.startDrag(dragData,  // the data to be dragged
                        new View.DragShadowBuilder(view), // the drag shadow builder
                        null,
                        0          // flags (not currently used, set to 0)
                );
                return true;
            }
        });


        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                if (mActionMode != null) {

                    adapter.getImageItem(i).toggleSelected();
                    ((CheckableLayout) (view)).toggle();

                    int selectCount = adapter.getSelectedCount();
                    //mActionMode.setTitle("Seleccionar elementos");
                    TextView tv = mActionMode.getCustomView().findViewById(R.id.tv_items_selected);

                    String str;
                    switch (selectCount) {
                        case 1:
                            str = String.format(getResources().getString(R.string.one_item_selected));
                            tv.setText(str);
                            break;
                        default:
                            str = String.format(getResources().getString(R.string.n_items_selected), String.valueOf(selectCount));
                            tv.setText(str);
                            break;
                    }

                    int nChecked = 0;
                    for (int k = 0; k < adapter.getCount(); k++) {
                        if (adapter.getImageItem(k).isSelected()) {
                            nChecked++;
                        }
                    }

                    if (nChecked == adapter.getCount()) {
                        // seleccionar all
                        btnSelectAll.setText(R.string.unselect_all_items);
                        checkAllMode = false;
                    }

                    if (nChecked < adapter.getCount() && !checkAllMode) {
                        // seleccionar all
                        btnSelectAll.setText(R.string.select_all_items);
                        checkAllMode = true;
                    }
                }

                return;
            }
        });
        return true;
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_pick_image_fragment, menu);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.action_select_items) {

            // si la grilla existe, entrar en modo seleccion de items
            if (gridview != null && gridview.getCount() > 0) {
                return setSelectionMode();
            } else {
                return false;
            }

        }

        if (item.getItemId() == R.id.action_pdf_settings) {
            // launch settings activity
            startActivity(new Intent(this.getActivity(), PDFPreferencesActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    public void checkAllItems() {

        if (mActionMode != null && gridview != null) {

            for (int i = 0; i < adapter.getCount(); i++) {
                adapter.getImageItem(i).setSelected(checkAllMode);
            }

            // mostrar como chequeados los views visibles
            int visibleChildCount = (gridview.getLastVisiblePosition() - gridview.getFirstVisiblePosition()) + 1;
            for (int i = 0; i < visibleChildCount; i++) {
                ((CheckableLayout) gridview.getChildAt(i)).setChecked(checkAllMode);
            }

            TextView tv = mActionMode.getCustomView().findViewById(R.id.tv_items_selected);
            int count = adapter.getSelectedCount();

            String str;
            if (count == 1) {
                str = String.format(getResources().getString(R.string.one_item_selected));
            } else {
                str = String.format(getResources().getString(R.string.n_items_selected), String.valueOf(count));
            }
            tv.setText(str);

            if (checkAllMode) {
                btnSelectAll.setText(R.string.unselect_all_items);
                checkAllMode = false;
            } else {
                btnSelectAll.setText(R.string.select_all_items);
                checkAllMode = true;
            }
        }
    }


    private AdapterView.OnItemClickListener defaultGridItemClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
            ((ScanActivity) getActivity()).editImage(i);
        }
    };


    private AdapterView.OnDragListener gridviewOnDragListener = new AdapterView.OnDragListener() {
        @Override
        public boolean onDrag(View gv, DragEvent event) {
            int height = gv.getHeight();

            switch (event.getAction()) {
                case DragEvent.ACTION_DRAG_STARTED:
                    return true;

                case DragEvent.ACTION_DRAG_LOCATION:
                    int THRESHHOLD = 200;
                    int OFFSET = 10;
                    float y = event.getY();
                    Log.d("diff", String.valueOf(height - y));
                    if (height - y < THRESHHOLD) {
                        ((GridView) gv).smoothScrollByOffset(OFFSET);
                    } else if (height - y > height - THRESHHOLD) {
                        ((GridView) gv).smoothScrollByOffset(-OFFSET);
                    }
                    return true;

                case DragEvent.ACTION_DROP:

                    // Gets the item containing the dragged data
                    ClipData.Item item = event.getClipData().getItemAt(0);
                    String[] data = item.getText().toString().split(",");

                    // posicion dentro del arrayAdapter de la vista desde donde se arrastra
                    int posOri = Integer.parseInt(data[0]);

                    // posicion dentro del gridview de la vista desde donde se arrastra
                    int idOri = posOri - gridview.getFirstVisiblePosition();

                    // Gets the text data from the item.
                    //int posDest = gridview.getPositionForView(v);
                    int posDest = gridview.pointToPosition((int) event.getX(), (int) event.getY());

                    int idDst = posDest - gridview.getFirstVisiblePosition();
                    View v = gridview.getChildAt(idDst);

                    if (v == null) return true;

                    ImageItem oDst = imageItems.get(posDest);
                    ImageItem oSrc = imageItems.get(posOri);
                    oDst.setItem_header("" + posOri);
                    oSrc.setItem_header("" + posDest);

                    imageItems.set(posDest, oSrc);
                    imageItems.set(posOri, oDst);

                    // swap uriList
                    ArrayList<String> uriList = ((ScanActivity) getActivity()).uriList;
                    String uriDst = uriList.get(posDest);
                    String uriSrc = uriList.get(posOri);
                    uriList.set(posDest, uriSrc);
                    uriList.set(posOri, uriDst);

                    // swap Original uri list
                    ArrayList<String> originalUriList = ((ScanActivity) getActivity()).originalUriList;
                    String uriDest = originalUriList.get(posDest);
                    String uriSource = originalUriList.get(posOri);
                    originalUriList.set(posDest, uriSource);
                    originalUriList.set(posOri, uriDest);

                    //swap de las preferencias
                    ArrayList<PagePreference> pagesPreferences = ((ScanActivity) getActivity()).pagesPreferences;
                    PagePreference prefDest = pagesPreferences.get(posDest);
                    PagePreference prefSource = pagesPreferences.get(posOri);
                    pagesPreferences.set(posDest, prefSource);
                    pagesPreferences.set(posOri, prefDest);

                    // actualizar
                    boolean checked = oSrc.isSelected();
                    //CheckableLayout cv = (CheckableLayout) gridview.getChildAt(posDest);
                    if (checked) {
                        ((CheckableLayout) v).setChecked(true);
                    } else {
                        ((CheckableLayout) v).setChecked(false);
                    }

                    CheckableLayout cv = (CheckableLayout) gridview.getChildAt(idOri);
                    if (cv != null) { // si la posicion de origen ya no es visible(por scroll) cv es null
                        checked = oDst.isSelected();
                        if (checked) {
                            cv.setChecked(true);
                        } else {
                            cv.setChecked(false);
                        }
                    }

                    adapter.notifyDataSetChanged();
                    return true;
            }
            return true;
        }
    };


    public static class createPDFTask extends AsyncTask<Void, Void, Boolean> {

        WeakReference<ScanActivity> mActivity;

        String path;
        ArrayList<String> uriList;
        ArrayList<String> originalUriList;
        ArrayList<PagePreference> pagesPreferences;

        Uri pdfUri;
        String docEditId = "";
        Integer ppi;
        String depthcolor;
        String fileDesc;

        public createPDFTask(ScanActivity activity, String path, ArrayList<String> uriList, ArrayList<String> originalUriList ,ArrayList<PagePreference> pagesPreferences, String fileDesc, Integer ppi, String depthcolor) {
            this.mActivity = new WeakReference<>(activity);
            this.path = path;
            this.uriList = uriList;
            this.originalUriList = originalUriList;
            this.pagesPreferences = pagesPreferences;
            this.ppi = ppi;
            this.depthcolor = depthcolor;
            this.fileDesc = fileDesc;
        }

        protected void onPreExecute() {
            super.onPreExecute();
        }

        protected Boolean doInBackground(Void... params) {

            byte[] docBytes = Utils.createPDFDocument(mActivity.get(), uriList, pagesPreferences, ppi, depthcolor);
            docEditId = Utils.getDocEditId(uriList, originalUriList, pagesPreferences, fileDesc, mActivity.get());
            File outputFile = new File(path);
            outputFile.getParentFile().mkdirs();
            FileOutputStream out = null;

            try {
                out = new FileOutputStream(outputFile);
                out.write(docBytes);
                out.close();
                pdfUri = Uri.fromFile(outputFile);

            } catch (Exception e) {
                try {
                    out.close();
                } catch (Exception ioexcep) {
                }
                return false;
            }

            return true;
        }


        protected void onPostExecute(Boolean result) {
            ScanActivity me = mActivity.get();
            if (me == null)
                return;
            if (pdfUri == null) {
                Toast.makeText(me, "Error: no se pudo generar el pdf.", Toast.LENGTH_LONG).show();
                return;
            }
            Intent data = new Intent();
            data.putExtra("pdf_uri", pdfUri);
            data.putExtra("doc_edit_id", docEditId);
            me.setResult(Activity.RESULT_OK, data);
            me.finish();
        }
    }


    public void showHints() {

        SharedPreferences prefs = getActivity().getSharedPreferences(
                PREFS_NAME, Context.MODE_PRIVATE);
        if(prefs.getBoolean("hint_already_showed",false) == true){
            return;
        }
        optionsHintLayout.setVisibility(View.VISIBLE);
        buttonsHintLayout.setVisibility(View.VISIBLE);
    }


    public void hideHints() {
        try {
            if (optionsHintLayout.getVisibility() == View.VISIBLE) {
                optionsHintLayout.setVisibility(View.GONE);
                buttonsHintLayout.setVisibility(View.GONE);

                // ya no se va a mostrar mas
                SharedPreferences prefs = getActivity().getSharedPreferences(
                        PREFS_NAME, Context.MODE_PRIVATE);

                SharedPreferences.Editor editor = prefs.edit();
                editor.putBoolean("hint_already_showed", true);
                editor.commit();
            }

        } catch (Exception e) {
        }
    }

}