/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 */

package com.example.facundo.myapplication.customviews;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;

import com.example.facundo.myapplication.MainActivity;
import com.example.facundo.myapplication.R;

/**
 * Created by flezcano on 10/04/2018.
 */



public class PickDocDialog extends DialogFragment {

    MainActivity parent;

    public PickDocDialog(){
    //setStyle(R.style.Dialog,getTheme());
    }



    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        Dialog dialog = super.onCreateDialog(savedInstanceState);
        // request a window without the title
        dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
        return dialog;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.dialog_pick_document, container, false);
        final PickDocDialog me = this;

        Button cancel = (Button) root.findViewById(R.id.cancel_button);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                me.dismiss();
            }
        });

        Button camera = (Button) root.findViewById(R.id.take_from_camera_button);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //parent.getDocumentFromScan2();
                me.dismiss();
            }
        });
        Button gallery = (Button) root.findViewById(R.id.choose_from_gallery_button);
        gallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //parent.editDocumentFromScan2();
                me.dismiss();
            }
        });

        return root;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.parent = (MainActivity) activity;
    }
}
