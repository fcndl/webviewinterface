/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Impronta dev <improntatest@gmail.com>, March 2018
 */

package com.example.facundo.myapplication;

import org.apache.commons.io.FileUtils;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.Toast;

import com.example.facundo.myapplication.customviews.PickDocDialog;
import com.example.facundo.myapplication.interfaces.*;
import com.improntasolutions.nvmobilescan.NvMobileScan;
import com.improntasolutions.nvmobilescan.NvMobileScanChooser;
import com.improntasolutions.nvmobilescan.ScanActivity;
import com.improntasolutions.nvmobilescan.ScanConstants;


import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class MainActivity extends AppCompatActivity  {
    String TAG = getClass().getName();

    WebView myWebView;
    MyChromeWebViewClient chromeWebViewClient;
    NvMobileScanChooser mscanchooser;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //PackageInfo webViewPackageInfo = WebView.getCurrentWebViewPackage();
        //Log.d("MY_APP_TAG", "WebView version: " + webViewPackageInfo.versionName);

        myWebView = findViewById(R.id.webview);
        WebSettings webSettings = myWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        // desde la pagina web, podemos preguntar
        // si el agente es WEB_APP para saber que la pagina
        // se esta ejecutando desde el cliente android
        webSettings.setUserAgentString("WEB_APP");

        // zoom alejado
        //webSettings.setLoadWithOverviewMode(true);
        //webSettings.setUseWideViewPort(true);


        //webSettings.setSupportMultipleWindows(true);


        // para que funcione window.localStorage
        webSettings.setDomStorageEnabled(true);

        // el código javascript dentro del webview podrá acceder
        // a los metodos declarados con la annotation JavascriptInterface.
        // Al objeto javascript se lo nombra como "Android"
        myWebView.addJavascriptInterface(this, "nvInterOP");


        // cargar sitio desde internet
        String myHost;
        String myHomeUrl;
        try {
            myHost = Util.getProperty("site_HOST", getApplicationContext());
            myHomeUrl = Util.getProperty("site_HOME_URL", getApplicationContext());
        } catch (IOException e) {
            Log.e(TAG, e.getMessage());
            return;
        }
        myWebView.setWebViewClient(new MyWebViewClient(myHost,this));



        chromeWebViewClient = new MyChromeWebViewClient(myHost,this);
        myWebView.setWebChromeClient(chromeWebViewClient);


        myWebView.loadUrl(myHomeUrl);


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }


        // cargar el sitio web desde assets
        //myWebView.loadUrl("file:///android_asset/site/index.html");


        // cargar el objeto nvmobilescan para la llamada a funciones de scan de documentos
        mscanchooser = new NvMobileScanChooser(this);

        // cargar los ppi por defecto
        mscanchooser.setDefault_ppi(300);

        // path por defecto donde se guardara el documento escaneado
        mscanchooser.setDefault_path("/storage/emulated/0/Download/myfolder");


        mscanchooser.clearCacheFolder();
        mscanchooser.clearCacheFolderOnInit = false;

    }



    @JavascriptInterface
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        // Check if the key event was the Back button and if there's history
        if ((keyCode == KeyEvent.KEYCODE_BACK) && myWebView.canGoBack()) {
            myWebView.goBack();
            return true;
        }
        // If it wasn't the Back key or there's no web page history, bubble up to the default
        // system behavior (probably exit the activity)
        return super.onKeyDown(keyCode, event);
    }


    NvMobileScan.ScanResultCallback callback = new NvMobileScan.ScanResultCallback() {
        @Override
        public void run(Object data) {
            HashMap<String, Object> m = (HashMap<String,Object>)data;
            Toast.makeText(getApplicationContext(),m.get("fileName").toString(), Toast.LENGTH_LONG).show();
        }
    };




    @JavascriptInterface
    public String getDocumentFromChooser() {
         return mscanchooser.getDocumentFromChooser();
    }

    @JavascriptInterface
    public String getDocumentFromChooser(int inputPpi, String path, String inputFileDesc ) {
        return mscanchooser.getDocumentFromChooser(inputPpi, path, inputFileDesc);
    }

    @JavascriptInterface
    public String getDocumentFromChooser(String inputFileDesc ) {
        return mscanchooser.getDocumentFromChooser(null, null, inputFileDesc);
    }


    @JavascriptInterface
    public String editScannedDocument(String docEditId) {
        return mscanchooser.editScannedDocument(docEditId);
    }




    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        mscanchooser.onActivityResult(requestCode, resultCode, data);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //mscanchooser.onRequestPermissionsResult(requestCode,permissions,grantResults);
    }



    @Override
    public void onDestroy(){
        super.onDestroy();
        try {
            //mscanchooser.release();
        }catch(Exception e){
        }
    }




}
