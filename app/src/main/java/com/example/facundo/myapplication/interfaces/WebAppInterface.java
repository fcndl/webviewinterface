/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 */

package com.example.facundo.myapplication.interfaces;

import android.webkit.JavascriptInterface;

/**
 * Created by flezcano on 10/04/2018.
 */

public interface WebAppInterface {

    @JavascriptInterface
     String getDocumentFromScan(String inputPpi, String path, String inputFileDesc);

    @JavascriptInterface
    public String getDocumentFromScan();

    @JavascriptInterface
    public String editDocumentFromScan(String inputPpi, String path, String inputFileDesc);


    @JavascriptInterface
    public String editDocumentFromScan();



    @JavascriptInterface
    public String editScannedDocument(String docEditId);




}
