/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 * Unauthorized copying of this file, via any medium is strictly prohibited
 * Proprietary and confidential
 * Written by Impronta dev <improntatest@gmail.com>, March 2018
 *
 */

package com.example.facundo.myapplication;



        import android.net.Uri;
        import android.webkit.ValueCallback;
        import android.webkit.WebChromeClient;
        import android.webkit.WebView;

/**
 * Created by facundo on 24/03/2018.
 */

public class MyChromeWebViewClient extends WebChromeClient {
    String TAG = getClass().getName();
    String myHost;
    MainActivity mContext;

    MyChromeWebViewClient(String myHost, MainActivity ctx) {
        this.myHost = myHost;
        this.mContext = ctx;
    }


    ValueCallback<Uri[]>  mUploadMessage;


    @Override
    public boolean onShowFileChooser(WebView webView, ValueCallback<Uri[]> filePathCallback, FileChooserParams fileChooserParams) {
        openFileChooser(filePathCallback, "");
        return super.onShowFileChooser(webView, filePathCallback, fileChooserParams);
    }



    @SuppressWarnings("unused")

    public void openFileChooser(ValueCallback<Uri[]> uploadMsg, String AcceptType, String capture) {
        this.openFileChooser(uploadMsg);
    }

    @SuppressWarnings("unused")
    public void openFileChooser(ValueCallback<Uri[]> uploadMsg, String AcceptType) {
        this.openFileChooser(uploadMsg);
    }

    public void openFileChooser(ValueCallback<Uri[]> uploadMsg) {
        mUploadMessage = uploadMsg;
        //mContext.pickFile();
    }

    public void setFileData(String base64str) {
        Uri uri = Uri.parse(base64str);

        mUploadMessage.onReceiveValue(new Uri[]{uri});
        mUploadMessage = null;
    }
}