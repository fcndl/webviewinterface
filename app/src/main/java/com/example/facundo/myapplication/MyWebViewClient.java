/*
 * Copyright (C) Impronta Solutions SA - All Rights Reserved
 *  Unauthorized copying of this file, via any medium is strictly prohibited
 *  Proprietary and confidential
 *  Written by Impronta dev <improntatest@gmail.com>, March 2018
 */

package com.example.facundo.myapplication;

import android.content.Intent;
import android.net.Uri;
import android.net.http.SslError;
import android.webkit.SslErrorHandler;
import android.webkit.WebView;
import android.webkit.WebViewClient;

/**
 * Created by facundo on 24/03/2018.
 */

public class MyWebViewClient extends WebViewClient {
    String TAG = getClass().getName();
    String myHost;
    MainActivity mContext;

    MyWebViewClient(String myHost, MainActivity ctx) {
        this.myHost = myHost;
        this.mContext = ctx;
    }

    @Override
    public boolean shouldOverrideUrlLoading(WebView view, String url) {

        if (Uri.parse(url).getHost().equals(myHost)) {
            // This is my web site, so do not override; let my WebView load the page
            return false;
        }
        // Otherwise, the link is not for a page on my site, so launch another Activity that handles URLs
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
        mContext.startActivity(intent);
        return true;
    }

    @Override
    public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
        handler.proceed(); // Ignore SSL certificate errors
    }


}